from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import get_jwt_identity, jwt_required, get_jwt
from src.database import Propertydata, db, Country, City, State, Unitformat, Propertyfloordata, Propertyleases
#from flasgger import swag_from

propertys = Blueprint("propertys", __name__, url_prefix="/api/v1/propertys")

@propertys.route('/PropData/', methods=['GET'])
@jwt_required()
def handle_allPropData():

    if request.method == 'GET':
        UserID = request.get_json().get('UserID', '')

        data = []
        items = db.session.query(Propertydata.nooffloors, Propertydata.noofaptperfloors, Propertydata.postalcode, Propertydata.id, Propertydata.name, Propertydata.owner, Propertydata.suffix, Propertydata.address, City.name.label('city_name'), State.name.label('state_name'), Country.name.label('country_name')).join(City, City.id == Propertydata.city_id).join(State, State.id == Propertydata.state_id).join(Country, Country.id == Propertydata.country_id).filter(Propertydata.user_id == UserID).all()

        for item in items:
            data.append({
                "id" : item.id, 
                "prop_name" : item.name, 
                "prop_owner" : item.owner, 
                "prop_suffix" : item.suffix, 
                "prop_address" : item.address,
                "cityname" : item.city_name, 
                "postalcode" : item.postalcode,
                "statename" : item.state_name, 
                "countryname" : item.country_name,
                "prop_nooffloors" : item.nooffloors, 
                "prop_noofaptperfloors" : item.noofaptperfloors
            })

        return jsonify(data), HTTP_200_OK

@propertys.route('/', methods=['POST', 'GET'])
@jwt_required()
def handle_propertys():
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    #if current_user == 0:
        #return jsonify({
        #    'error': "Login not exists"
        #}), HTTP_200_OK

    if request.method == 'POST':

        if current_user == 0:
            current_user = request.get_json().get('current_user', '')

        name = request.get_json().get('name', '')
        owner = request.get_json().get('owner', '')
        suffix = request.get_json().get('suffix', '')
        address = request.get_json().get('address', '')
        city_id = request.get_json().get('city_id', '')
        state_id = request.get_json().get('state_id', '')
        country_id = request.get_json().get('country_id', '')
        postalcode = request.get_json().get('postalcode', '')
        sorting_id = request.get_json().get('sorting_id', '0')
        nooffloors = request.get_json().get('nooffloors', '')
        user_current_manage = 0
        noofaptperfloors = request.get_json().get('noofaptperfloors', '')
        unitformat_id = request.get_json().get('unitformat_id', '')

        if len(suffix) > 3:
            return jsonify({'error': "Property suffix is only 3 Character allow"}), HTTP_400_BAD_REQUEST

        if not nooffloors.isnumeric() or " " in nooffloors:
            return jsonify({'error': "No of Floors should be number, also no spaces"}), HTTP_400_BAD_REQUEST

        if not noofaptperfloors.isnumeric() or " " in noofaptperfloors:
            return jsonify({'error': "No of Apartments per Floors should be number, also no spaces"}), HTTP_400_BAD_REQUEST

        property = Propertydata(name=name, 
                            owner=owner, 
                            suffix=suffix, 
                            address=address, 
                            city_id=city_id, 
                            state_id=state_id, 
                            country_id=country_id, 
                            postalcode=postalcode, 
                            sorting_id=sorting_id,
                            nooffloors=nooffloors, 
                            user_current_manage=user_current_manage, 
                            noofaptperfloors=noofaptperfloors, 
                            unitformat_id=unitformat_id, 
                            user_id=current_user)
        db.session.add(property)
        db.session.commit()

        return jsonify({
            'id': property.id,
            'name': property.name,
            'owner': property.owner,
            'suffix': property.suffix,
            'address': property.address,
            'city_id': property.city_id,
            'state_id': property.state_id,
            'country_id': property.country_id,
            'postalcode': property.postalcode,
            'sorting_id': property.sorting_id,
            'nooffloors': property.nooffloors,
            'user_current_manage': property.user_current_manage,
            'noofaptperfloors': property.noofaptperfloors,
            'unitformat_id': property.unitformat_id,
            'created_at': property.created_at,
            'updated_at': property.updated_at,
        }), HTTP_201_CREATED

    else:
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 5, type=int)

        if current_user == 0:
            current_user = request.args.get('current_user')
        
        removeFlag = request.args.get('removeFlag')

        if (removeFlag == "all"):
            propertys = Propertydata.query.filter_by(
            user_id=current_user).order_by(Propertydata.user_current_manage.desc(), Propertydata.sorting_id).paginate(page=page, per_page=per_page)
        else:
            propertys = Propertydata.query.filter_by(
            user_id=current_user, remove_flag=0).order_by(Propertydata.user_current_manage.desc(), Propertydata.sorting_id).paginate(page=page, per_page=per_page)

        arrCountry = {}
        for item in Country.query.all():
            arrCountry.update({item.id : item.name})

        arrState = {}
        for item in State.query.all():
            arrState.update({item.id : item.name})

        arrCity = {}
        for item in City.query.all():
            arrCity.update({item.id : item.name})

        arrUnitformat = {}
        for item in Unitformat.query.all():
            arrUnitformat.update({item.id : str(item.floorsno) + item.middlename + str(item.aptno)})

        data = []

        for property in propertys.items:
            data.append({
                'id': property.id,
                'name': property.name,
                'owner': property.owner,
                'suffix': property.suffix,
                'address': property.address,
                'postalcode': property.postalcode,
                'RemoveFlag': property.remove_flag,
                'FavoriteFlag': property.user_current_manage,
                'Country': arrCountry[property.country_id],
                'State': arrState[property.state_id],
                'City': arrCity[property.city_id],
                'Unitformat': arrUnitformat[property.unitformat_id],
            })
        
        pageList = []
        for i in range(1, (propertys.pages + 1)):
            currPage = False
            if (propertys.page == i):
                currPage = True

            pageList.append({
                'currPage': currPage,
                'page_No': i
            });


        meta = {
            "page": propertys.page,
            'pages': propertys.pages,
            'total_count': propertys.total,
            'prev_page': propertys.prev_num,
            'next_page': propertys.next_num,
            'has_next': propertys.has_next,
            'has_prev': propertys.has_prev,
        }

        return jsonify({'data': data, "meta": meta, "pageList": pageList}), HTTP_200_OK

@propertys.get("/<int:id>/<int:user_id>")
@jwt_required()
def get_property(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    #if current_user == 0:
    #    return jsonify({
    #        'error': "Login not exists"
    #    }), HTTP_200_OK
    if current_user == 0:
        current_user = user_id

    arrCountry = {}
    for item in Country.query.all():
        arrCountry.update({item.id : item.name})

    arrState = {}
    for item in State.query.all():
        arrState.update({item.id : item.name})

    arrCity = {}
    for item in City.query.all():
        arrCity.update({item.id : item.name})

    arrUnitformat = {}
    for item in Unitformat.query.all():
        arrUnitformat.update({item.id : str(item.floorsno) + item.middlename + str(item.aptno)})

    property = Propertydata.query.filter_by(id=id).first()#user_id=current_user, 

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND


    Floordata = []
    items = db.session.query(Propertyfloordata.floorsno, Propertyfloordata.aptno).filter(Propertyfloordata.property_id == id).order_by(Propertyfloordata.floorsno, Propertyfloordata.aptno).all()

    for item in items:
        Floordata.append({
            "floorsno" : item.floorsno, 
            "aptno" : item.aptno
        })

    return jsonify({
        'Floordata': Floordata,
        'propertyID': property.id,
        'propertyName': property.name,
        'propertyOwner': property.owner,
        'propertyNameSuffix': property.suffix,
        'propertyAddress': property.address,
        'postalCode': property.postalcode,
        'country': property.country_id,
        'state': property.state_id,
        'city': property.city_id,
        'sorting_id': property.sorting_id,
        'numberOfFloors': property.nooffloors,
        'numberOfApartments': property.noofaptperfloors,
        'unitesNumberFormat': property.unitformat_id,
        'unitesNoFormatText': arrUnitformat[property.unitformat_id],
    }), HTTP_200_OK

@propertys.delete("/<int:id>/<int:user_id>")
@jwt_required()
def delete_property(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    property = Propertydata.query.filter_by(id=id).first()#user_id=current_user, 

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    db.session.query(Propertyfloordata).filter_by(property_id=id).delete(synchronize_session=False)
    db.session.commit()

    db.session.delete(property)
    db.session.commit()

    return jsonify({}), HTTP_204_NO_CONTENT

@propertys.put("/toggleArchived/<int:id>/<int:user_id>")
@propertys.patch("/toggleArchived/<int:id>/<int:user_id>")
@jwt_required()
def toggleArchived_property(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    property = Propertydata.query.filter_by(id=id).first()#user_id=current_user, 

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    if (property.remove_flag == 1):
        property.remove_flag = '0'
    else:
        property.remove_flag = '1'

    db.session.commit()

    return jsonify({
        'id': property.id,
        'name': property.name,
        'suffix': property.suffix,
        'remove_flag': property.remove_flag,
    }), HTTP_200_OK

@propertys.put("/toggleFavorite/<int:id>/<int:user_id>")
@propertys.patch("/toggleFavorite/<int:id>/<int:user_id>")
@jwt_required()
def toggleFavorite_property(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    property = Propertydata.query.filter_by(id=id).first()#user_id=current_user, 

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    if (property.user_current_manage == 1):
        property.user_current_manage = '0'
    else:
        property.user_current_manage = '1'

    db.session.commit()

    return jsonify({
        'id': property.id,
        'name': property.name,
        'suffix': property.suffix,
        'user_current_manage': property.user_current_manage,
    }), HTTP_200_OK

@propertys.put('/<int:id>/<int:user_id>')
@propertys.patch('/<int:id>/<int:user_id>')
@jwt_required()
def editproperty(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    #if current_user == 0:
    #    return jsonify({
    #        'error': "Login not exists"
    #    }), HTTP_200_OK
    if current_user == 0:
        current_user = user_id

    property = Propertydata.query.filter_by(id=id).first()#user_id=current_user, 

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    name = request.get_json().get('name', '')
    owner = request.get_json().get('owner', '')
    suffix = request.get_json().get('suffix', '')
    address = request.get_json().get('address', '')
    city_id = request.get_json().get('city_id', '')
    state_id = request.get_json().get('state_id', '')
    country_id = request.get_json().get('country_id', '')
    postalcode = request.get_json().get('postalcode', '')
    sorting_id = request.get_json().get('sorting_id', '0')
    #nooffloors = request.get_json().get('nooffloors', '')
    #noofaptperfloors = request.get_json().get('noofaptperfloors', '')
    #unitformat_id = request.get_json().get('unitformat_id', '')

    property.name = name
    property.owner = owner
    property.suffix = suffix
    property.address = address
    property.city_id = city_id
    property.state_id = state_id
    property.country_id = country_id
    property.postalcode = postalcode
    property.sorting_id = sorting_id
    #property.nooffloors = nooffloors
    #property.noofaptperfloors = noofaptperfloors
    #property.unitformat_id = unitformat_id

    db.session.commit()

    return jsonify({
        'id': property.id,
        'name': property.name,
        'suffix': property.suffix,
        'address': property.address,
        'city_id': property.city_id,
        'state_id': property.state_id,
        'country_id': property.country_id,
        'postalcode': property.postalcode,
        'sorting_id': property.sorting_id,
        'nooffloors': property.nooffloors,
        'user_current_manage': property.user_current_manage,
        'noofaptperfloors': property.noofaptperfloors,
        'unitformat_id': property.unitformat_id,
        'created_at': property.created_at,
        'updated_at': property.updated_at,
    }), HTTP_200_OK

@propertys.route('/alldata', methods=['GET'])
@jwt_required()
#@swag_from("./docs/propertys/stats.yaml")
def get_alldata():
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = request.args.get('current_user')

    arrCountry = {}
    for item in Country.query.all():
        arrCountry.update({item.id : item.name})

    arrState = {}
    for item in State.query.all():
        arrState.update({item.id : item.name})

    arrCity = {}
    for item in City.query.all():
        arrCity.update({item.id : item.name})

    arrUnitformat = {}
    for item in Unitformat.query.all():
        arrUnitformat.update({item.id : str(item.floorsno) + item.middlename + str(item.aptno)})

    data = []
    items = Propertydata.query.filter_by(user_id=current_user).all()
    for item in items:
        new_link = {
            'suffix': item.suffix,
            'name': item.name,
            'id': item.id,
            'address': item.address,
            'postalcode': item.postalcode,
            'Country': arrCountry[item.country_id],
            'State': arrState[item.state_id],
            'City': arrCity[item.city_id],
            'Unitformat': arrUnitformat[item.unitformat_id],
        }

        data.append(new_link)

    #items = db.session.query(Propertydata.suffix, Propertydata.name, Propertydata.id, Unitformat.floorsno , Unitformat.middlename , Unitformat.aptno).filter_by(user_id=current_user).join(Unitformat).all()
    #for item in items:
    #    new_link = {
    #        'suffix': item[0],
    #        'name': item[1],
    #        'id': item[2],
    #        'cityName': str(item[3]) + item[4] + str(item[5]),
    #    }
    #    data.append(new_link)

    return jsonify({'data': data}), HTTP_200_OK

@propertys.get("/allcountry")
@jwt_required()
def get_allcountry():
    data = []
    items = Country.query.filter_by().all()
    for item in items:
        data.append({"country_id" : item.id, "country_name" : item.name })

    return jsonify(data), HTTP_200_OK

@propertys.get("/allcity")
@jwt_required()
def get_allcity():
    data = []
    #items = City.query.filter_by().all()
    items = db.session.query(City.id, City.name, State.name.label('stateName'), Country.name.label('countryName')).join(State, State.id == City.state_id).join(Country, Country.id == State.country_id).filter_by().all()
    for item in items:
        data.append({"city_id" : item.id, "city_name" : item.name, "stateName" : item.stateName, "countryName" : item.countryName })

    return jsonify(data), HTTP_200_OK

@propertys.get("/allstate")
@jwt_required()
def get_allstate():
    data = []
    #items = State.query.filter_by().all()
    items = db.session.query(State.id, State.name, Country.id.label('countryID') ,Country.name.label('countryName')).join(Country, Country.id == State.country_id).filter_by().all()
    for item in items:
        data.append({"state_id" : item.id, "state_name" : item.name, "countryID" : item.countryID, "countryName" : item.countryName })

    return jsonify(data), HTTP_200_OK

@propertys.get("/allunit")
@jwt_required()
def get_allunit():
    data = []
    items = Unitformat.query.filter_by().all()
    for item in items:
        data.append({"unit_id" : item.id, "unit_name" : str(item.floorsno) + item.middlename + str(item.aptno) })

    return jsonify(data), HTTP_200_OK

@propertys.get("/aptdata/<int:id>/<int:floorsno>")
@jwt_required()
def get_property_apt(id, floorsno):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        return jsonify({
            'error': "Login not exists"
        }), HTTP_200_OK

    property = Propertydata.query.filter_by(user_id=current_user, id=id).first()

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    arrUnitformat = {}
    for item in Unitformat.query.all():
        if (property.unitformat_id==item.id):
            arrUnitformat.update({
                'id' : item.id,
                'format' : str(item.floorsno) + item.middlename + str(item.aptno),
                'floorsno' : item.floorsno,
                'middlename' : item.middlename,
                'aptno' : item.aptno,
            })

    data = []
    for i in range(1, property.nooffloors + 1):
        arrApt = {}
        if ((floorsno==i) or floorsno==0):
            for j in range(1, property.noofaptperfloors + 1):
                arrApt.update({j: str(i) + arrUnitformat['middlename'] + str(j)})#property.unitformat_id

            new_link = {
                'Floors': i,
                'Apt': arrApt,
            }
            data.append(new_link)

    return jsonify({
        'id': property.id,
        'name': property.name,
        'FloorsAptData': data,
    }), HTTP_200_OK

@propertys.get("/aptdatawithoutstore/<int:unitformat_id>/<int:nooffloors>/<int:noofaptperfloors>")
@jwt_required()
def get_apt_data(unitformat_id, nooffloors, noofaptperfloors):
    AptData = []
    FloorsData = []
    AllData = []
    arrUnitformat = {}
    for item in Unitformat.query.all():
        if (unitformat_id==item.id):
            arrUnitformat.update({
                'id' : item.id,
                'format' : str(item.floorsno) + item.middlename + str(item.aptno),
                'floorsno' : item.floorsno,
                'middlename' : item.middlename,
                'aptno' : item.aptno,
            })

    for i in range(1, nooffloors + 1):
        arrApt = []
        for j in range(1, noofaptperfloors + 1):
            arrApt.append(str(i) + arrUnitformat['middlename'] + str(j))

        new_link = {
            i: arrApt,
        }
        AptData.append(new_link)
        FloorsData.append(i)
        
        newLinkAll = [i,arrApt]
        AllData.append(newLinkAll)

    return jsonify({
        'FloorsData': FloorsData,
        'AptData': AptData,
        'AllData': AllData,
    }), HTTP_200_OK

@propertys.get("/manage/<int:user_id>")
@jwt_required()
def get_property_byparam(user_id):
    current_user = user_id

    arrCountry = {}
    for item in Country.query.all():
        arrCountry.update({item.id : item.name})

    arrState = {}
    for item in State.query.all():
        arrState.update({item.id : item.name})

    arrCity = {}
    for item in City.query.all():
        arrCity.update({item.id : item.name})

    arrUnitformat = {}
    for item in Unitformat.query.all():
        arrUnitformat.update({item.id : str(item.floorsno) + item.middlename + str(item.aptno)})

    data = []
    items = Propertydata.query.filter_by(user_id=current_user, user_current_manage='1').all()
    for item in items:
        new_link = {
            'suffix': item.suffix,
            'name': item.name,
            'address': item.address,
            'owner': item.owner,
            'postalcode': item.postalcode,
            'id': item.id,
            'Country': arrCountry[item.country_id],
            'State': arrState[item.state_id],
            'City': arrCity[item.city_id],
            'Unitformat': arrUnitformat[item.unitformat_id],
        }

        data.append(new_link)

    return jsonify({'data': data}), HTTP_200_OK

@propertys.put("/byLeaseCode")
@propertys.patch("/byLeaseCode")
@jwt_required()
def getPropertybyLeaseCode():
    leaseCode = request.json.get('LeaseCode', '')
    user_id = request.json.get('user_id', '')

    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    property = db.session.query(Propertydata).join(Propertyleases, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==leaseCode).first()

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'propertyID': property.id,
        'propertyName': property.name,
        'propertyNameSuffix': property.suffix,
    }), HTTP_200_OK

