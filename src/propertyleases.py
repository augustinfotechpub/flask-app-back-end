from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import City, State, Country, Propertyleases, Propertyleasestenant, Propertyleasesfile, Propertydata, db, Usercode, Notification, User, Userpaymethod, Siteconfigs, Billinghistory, Billinghistorysub
from datetime import date, datetime, timedelta
from sqlalchemy import func
from sqlalchemy.orm import load_only
from sqlalchemy import extract
from sqlalchemy import cast
import datetime
from dateutil import relativedelta
from sqlalchemy.engine import result
import json
import stripe
import time
from cryptography.fernet import Fernet

propertyleases = Blueprint("propertyleases", __name__, url_prefix="/api/v1/propertyleases")

#stripe.api_key = 'sk_test_51KWejvLsLWJIq13O6R1qK9vm18bxKoruKkxs85mNoPWgzcmd6fsueWPzjXkxGBcA7B5AxDwI94RaedGRBhbvXOID00ZQ6jy7cf'

@propertyleases.route('/', methods=['POST', 'GET'])
@jwt_required()
def addPropertyleases():
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = request.get_json().get('current_user', '')

    property_id = request.get_json().get('property_id', '')
    floorsno = request.get_json().get('floorsno', '')
    aptno = request.get_json().get('aptno', '')
    lease_code = request.get_json().get('lease_code', '')

    if (request.get_json().get('oldleaseCode', '')==''):
        file_storagekey = request.get_json().get('lease_code', '')
    else:
        file_storagekey = request.get_json().get('oldleaseCode', '')

    lease_price = request.get_json().get('lease_price', '')

    #lease_start_date = datetime(request.get_json().get('sYear', ''), request.get_json().get('sMonth', ''), request.get_json().get('sDate', '')) 
    #lease_end_date = datetime(request.get_json().get('eYear', ''), request.get_json().get('eMonth', ''), request.get_json().get('eDate', '')) 

    sYear = request.get_json().get('sYear', '')
    sMonth = request.get_json().get('sMonth', '')
    sDate = request.get_json().get('sDate', '') 
    eYear = request.get_json().get('eYear', '')
    eMonth = request.get_json().get('eMonth', '')
    eDate = request.get_json().get('eDate', '') 

    lease_start_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))
    lease_end_date = datetime.datetime(int(eYear), int(eMonth), int(eDate))

    dataInsert = Propertyleases(
                        property_id=property_id, 
                        floorsno=floorsno, 
                        aptno=aptno, 
                        file_storagekey=file_storagekey,
                        lease_code=lease_code, 
                        lease_price=lease_price, 
                        lease_start_date=lease_start_date, 
                        lease_end_date=lease_end_date, 
                        lease_notification_send=0, 
                        rent_notification_send=0, 
                        billing_flag_update=0, 
                        lease_stripe_charge_id='', 
                        user_id=current_user)
    db.session.add(dataInsert)
    db.session.commit()

    tenantsData = request.get_json().get('tenantsData', '')
    for tData in tenantsData:
        subDataInsert = Propertyleasestenant(
                            leases_id=dataInsert.id, 
                            tenant_fname=tData['firstName'], 
                            tenant_lname=tData['lastName'],
                            user_id=current_user)
        db.session.add(subDataInsert)
        db.session.commit()

    filesData = request.get_json().get('fileData', '')
    for fData in filesData:
        subDataInsert = Propertyleasesfile(
                            leases_id=dataInsert.id, 
                            file_name=fData['filepath'], 
                            user_id=current_user)
        db.session.add(subDataInsert)
        db.session.commit()

    return jsonify({
        'id': dataInsert.id,
        'user_id': dataInsert.user_id,
        'property_id': dataInsert.property_id,
        'floorsno': dataInsert.floorsno,
        'aptno': dataInsert.aptno,
        'created_at': dataInsert.created_at,
        'updated_at': dataInsert.updated_at,
    }), HTTP_201_CREATED
        
@propertyleases.put("/alldatabyaptno")
@jwt_required()
def get_alldata():
    pid = request.json.get('PropID', '')
    aptno = request.json.get('AptNo', '')
    current_user = int(request.json.get('UserID', ''))

    currDt = date.today()

    columns = [extract('month', Propertyleases.lease_end_date).label("le_month"), extract('day', Propertyleases.lease_end_date).label("le_day"), extract('year', Propertyleases.lease_end_date).label("le_year"), extract('month', Propertyleases.lease_start_date).label("ls_month"), extract('day', Propertyleases.lease_start_date).label("ls_day"), extract('year', Propertyleases.lease_start_date).label("ls_year"), Propertyleases.id, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.file_storagekey, Propertyleases.lease_code, Propertyleases.lease_price, Propertyleases.lease_start_date, Propertyleases.lease_end_date]

    CurrentData = []
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'ls_year': item.ls_year,
            'ls_month': item.ls_month,
            'ls_day': item.ls_day,
            'le_year': item.le_year,
            'le_month': item.le_month,
            'le_day': item.le_day,
            'ls_month_name': datetime.datetime(1, int(item.ls_month), 1).strftime("%B"),
            'le_month_name': datetime.datetime(1, int(item.le_month), 1).strftime("%B"),
            'TenantsData': TData,
        }

        CurrentData.append(new_link)

    PastData = []
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_start_date < currDt).filter(Propertyleases.lease_end_date < currDt).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'ls_year': item.ls_year,
            'ls_month': item.ls_month,
            'ls_day': item.ls_day,
            'le_year': item.le_year,
            'le_month': item.le_month,
            'le_day': item.le_day,
            'ls_month_name': datetime.datetime(1, int(item.ls_month), 1).strftime("%B"),
            'le_month_name': datetime.datetime(1, int(item.le_month), 1).strftime("%B"),
            'TenantsData': TData,
        }

        PastData.append(new_link)

    UpcomingData = []
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_start_date > currDt).filter(Propertyleases.lease_end_date > currDt).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'ls_year': item.ls_year,
            'ls_month': item.ls_month,
            'ls_day': item.ls_day,
            'le_year': item.le_year,
            'le_month': item.le_month,
            'le_day': item.le_day,
            'ls_month_name': datetime.datetime(1, int(item.ls_month), 1).strftime("%B"),
            'le_month_name': datetime.datetime(1, int(item.le_month), 1).strftime("%B"),
            'TenantsData': TData,
        }

        UpcomingData.append(new_link)

    AddNewLease = 0
    if (len(CurrentData) == 0 and len(UpcomingData) == 0):
        AddNewLease = 1

    cntData = Propertyleases.query.filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).filter(Propertyleases.lease_cancel==1).count()
    #.filter(Propertyleases.user_id == current_user)
    if (len(CurrentData) == cntData):
        AddNewLease = 1

    MaxData = db.session.query(Propertyleases).with_entities(func.max(Propertyleases.lease_end_date), extract('month', Propertyleases.lease_end_date).label("month_no"), extract('day', Propertyleases.lease_end_date).label("day_no"), extract('year', Propertyleases.lease_end_date).label("year_no"), Propertyleases.lease_propose_renewal, Propertyleases.id, Propertyleases.file_storagekey).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).first()
    #.filter(Propertyleases.user_id == current_user)

    MaxDateText = ''
    MaxDate = ''
    MaxID = 0
    MaxStorageKey = ''
    ProposeRenewalFlag = 0
    if MaxData[0]:
        #MaxDateText = datetime.datetime(1, int(MaxData[1]), 1).strftime("%B") + ' ' + str(MaxData[2]) + ' ' + str(MaxData[3])
        MaxDateText = (datetime.datetime.strptime(str(MaxData[0]), "%Y-%m-%d") + timedelta(days=1)).strftime('%b %d %Y')
        MaxDate = MaxData[0]
        ProposeRenewalFlag = MaxData[4]
        MaxID = MaxData[5]
        MaxStorageKey = MaxData[6]
    
    RenewalNewLease = 0
    if (ProposeRenewalFlag==1):
        RenewalNewLease = 1
           
    return jsonify({
        'AddNewLease': AddNewLease,
        'RenewalNewLease': RenewalNewLease,
        'MaxID': MaxID,
        'MaxStorageKey': MaxStorageKey,
        'MaxDate': MaxDate,
        'MaxDateText': MaxDateText,
        'CurrentData': CurrentData,
        'UpcomingData': UpcomingData,
        'PastData': PastData,
    }), HTTP_200_OK

@propertyleases.get("/<int:id>/<int:user_id>")
@jwt_required()
def get_leasedatabyid(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    #user_id=current_user
    
    LeaseData = Propertyleases.query.filter_by(id=id).first()

    if not LeaseData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND


    CardNumber = ''
    PaymentMethod = ''
    if ((LeaseData.lease_stripe_charge_id!='') and (LeaseData.lease_stripe_charge_id is not None)):
        propData = db.session.query(Propertydata).with_entities(Propertydata.id, User.stripe_api_key).join(Propertyleases, Propertyleases.property_id == Propertydata.id).join(User, User.id == Propertydata.user_id).filter(Propertyleases.id==id).first()
        stripe_api_key = propData.stripe_api_key

        if (stripe_api_key==''):
            return jsonify({'payment': 'error', 'message': 'Management Stripe Account not created' }), HTTP_200_OK

        stripe.api_key = stripe_api_key

        try:
            ChargeData = stripe.Charge.retrieve(
                LeaseData.lease_stripe_charge_id,
            )
            
            if (ChargeData.payment_method_details.card.last4):
                CardNumber = "**** **** **** " + ChargeData.payment_method_details.card.last4

            if (ChargeData.payment_method_details.card.brand):
                PaymentMethod = ChargeData.payment_method_details.card.brand

        except stripe.error.InvalidRequestError:
            CardNumber = ''
            PaymentMethod = ''
            #return jsonify({'payment': 'error', 'message': 'Management Stripe Account not created' }), HTTP_200_OK
            #err = ''
            #print("Code is: %s" % err.code)
            #payment_intent_id = err.payment_intent['id']
            #payment_intent = stripe.PaymentIntent.retrieve(payment_intent_id)
            #return jsonify({
            #    'id': propertyleases.id,
            #    'payment': 'Error',
            #}), HTTP_200_OK

    TData = []
    TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == LeaseData.id).all()
    for subitem in TenantsData:
        new_add = {
            'firstName': subitem.tenant_fname,
            'lastName': subitem.tenant_lname,
        }
        TData.append(new_add)
    
    FData = []
    FileData = db.session.query(Propertyleasesfile).filter(Propertyleasesfile.leases_id == LeaseData.id).all()
    for subitem in FileData:
        new_add = {
            'file_name': subitem.file_name,
        }
        FData.append(new_add)
    
    RentPeriod = (datetime.datetime.strptime(str(LeaseData.lease_start_date), "%Y-%m-%d")).strftime('%b. %d') + " - " + (datetime.datetime.strptime(str(LeaseData.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');
    if (LeaseData.lease_payment_date):
        PaidDate = (datetime.datetime.strptime(str(LeaseData.lease_payment_date), "%Y-%m-%d")).strftime('%d/%b/%Y')
    else:
        PaidDate = ''

    leaseEndDate = (datetime.datetime.strptime(str(LeaseData.lease_end_date), "%Y-%m-%d") + timedelta(days=1))
    leaseNewEndDate = last_day_of_month(datetime.date(leaseEndDate.year, leaseEndDate.month, leaseEndDate.day))

    return jsonify({
        'floorAptNo': LeaseData.aptno,
        'RentPeriod': RentPeriod,
        'RentCost': LeaseData.lease_price_pay,
        'PaidDate': PaidDate,

        'CardNumber': CardNumber,
        'PaymentMethod': PaymentMethod,

        'file_storagekey': LeaseData.file_storagekey,
        'leaseCode': LeaseData.lease_code,
        'currentLeasePrice': LeaseData.lease_price,
        'leaseStartDate': LeaseData.lease_start_date,
        'leaseEndDate': LeaseData.lease_end_date,
        'leaseNewStartDate': leaseEndDate,
        'leaseNewEndDate': leaseNewEndDate,
        'lease_cancel': LeaseData.lease_cancel,
        'lease_propose_renewal': LeaseData.lease_propose_renewal,
        'lease_payment_flag': LeaseData.lease_payment_flag,
        'id': LeaseData.id,
        'user_id': LeaseData.user_id,
        'property_id': LeaseData.property_id,
        'floorsno': LeaseData.floorsno,
        'TData': TData,
        'FData': FData,
    }), HTTP_200_OK

@propertyleases.put('/<int:id>/<int:user_id>')
@propertyleases.patch('/<int:id>/<int:user_id>')
@jwt_required()
def editpropertyleases(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    propertyleases = Propertyleases.query.filter_by(id=id).first()
    #user_id=current_user, 

    if not propertyleases:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    lease_code = request.get_json().get('lease_code', '')
    lease_price = request.get_json().get('lease_price', '')

    sYear = request.get_json().get('sYear', '')
    sMonth = request.get_json().get('sMonth', '')
    sDate = request.get_json().get('sDate', '') 
    eYear = request.get_json().get('eYear', '')
    eMonth = request.get_json().get('eMonth', '')
    eDate = request.get_json().get('eDate', '') 

    lease_start_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))
    lease_end_date = datetime.datetime(int(eYear), int(eMonth), int(eDate))

    #date_time_str = '18/09/19 00:00:00'
    #date_time_obj = datetime.datetime.strptime(date_time_str, '%d/%m/%y %H:%M:%S')


    propertyleases.lease_code = lease_code
    propertyleases.lease_price = lease_price
    propertyleases.lease_start_date = lease_start_date
    propertyleases.lease_end_date = lease_end_date

    db.session.commit()

    propUserID = propertyleases.user_id

    db.session.query(Propertyleasestenant).filter_by(leases_id=id).delete(synchronize_session=False)
    db.session.commit()

    tenantsData = request.get_json().get('tenantsData', '')
    for tData in tenantsData:
        subDataInsert = Propertyleasestenant(
                            leases_id=id, 
                            tenant_fname=tData['firstName'], 
                            tenant_lname=tData['lastName'],
                            user_id=propUserID)
        db.session.add(subDataInsert)
        db.session.commit()

    db.session.query(Propertyleasesfile).filter_by(leases_id=id).delete(synchronize_session=False)
    db.session.commit()

    filesData = request.get_json().get('fileData', '')
    for fData in filesData:
        subDataInsert = Propertyleasesfile(
                            leases_id=id, 
                            file_name=fData['filepath'], 
                            user_id=propUserID)
        db.session.add(subDataInsert)
        db.session.commit()

    return jsonify({
        'id': propertyleases.id,
        'user_id': propertyleases.user_id,
        'property_id': propertyleases.property_id,
        'floorsno': propertyleases.floorsno,
        'aptno': propertyleases.aptno,
    }), HTTP_200_OK

@propertyleases.put("/cancelLease/<int:id>/<int:user_id>")
@propertyleases.patch("/cancelLease/<int:id>/<int:user_id>")
@jwt_required()
def cancel_Lease(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    getData = Propertyleases.query.filter_by(user_id=current_user, id=id).first()

    if not getData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    #if (getData.lease_cancel == 1):
    #    getData.lease_cancel = '0'
    #else:
    
    getData.lease_cancel = '1'

    db.session.commit()

    return jsonify({
        'id': getData.id,
        'property_id': getData.property_id,
        'aptno': getData.aptno,
        'lease_cancel': getData.lease_cancel,
    }), HTTP_200_OK

@propertyleases.put("/RenewalLeasePropose/<int:id>/<int:renewal>/<int:user_id>")
@propertyleases.patch("/RenewalLeasePropose/<int:id>/<int:renewal>/<int:user_id>")
@jwt_required()
def Renewal_Lease_Propose(id, renewal, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    getData = Propertyleases.query.filter_by(id=id).first()
    #user_id=current_user, 

    if not getData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    """ if (getData.lease_propose_renewal == 1):
        getData.lease_propose_renewal = '0'
    else:
        getData.lease_propose_renewal = '1' """

    getData.lease_propose_renewal = renewal
    
    db.session.commit()

    return jsonify({
        'id': getData.id,
        'property_id': getData.property_id,
        'aptno': getData.aptno,
        'lease_propose_renewal': getData.lease_propose_renewal,
    }), HTTP_200_OK

@propertyleases.get("/aptdataRentUnpaid/<int:pid>/<int:userid>")
@jwt_required()
def get_aptdataRentUnpaid(pid, userid):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = userid

    #items = Propertyleases.query.filter_by(property_id=pid, lease_payment_flag='Pending', lease_cancel=0).group_by(Propertyleases.property_id, Propertyleases.lease_code, Propertyleases.floorsno, Propertyleases.aptno).order_by(Propertyleases.floorsno, Propertyleases.aptno).all()
    items = Propertyleases.query.filter_by(property_id=pid, lease_payment_flag='Pending', lease_cancel=0).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno).order_by(Propertyleases.floorsno, Propertyleases.aptno).all()
    #user_id=current_user, 

    RentAptData = []
    START = 0
    for idx, item in enumerate(items[START:], START):
        new_link = {
            'pid': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'id': idx,
        }
        RentAptData.append(new_link)

    cntRentAptData = Propertyleases.query.filter_by(property_id=pid, lease_payment_flag='Pending', lease_cancel=0).order_by(Propertyleases.floorsno, Propertyleases.aptno).count()
    #user_id=current_user, 



    #items = Propertyleases.query.filter_by(property_id=pid, lease_payment_flag='Pending', lease_cancel=0).group_by(Propertyleases.property_id, Propertyleases.lease_code, Propertyleases.floorsno, Propertyleases.aptno).all()
    items = db.session.query(Propertyleases.property_id, Propertyleases.lease_code, Propertyleases.floorsno, Propertyleases.aptno, Usercode.user_id).join(Usercode, Propertyleases.lease_code == Usercode.user_code).filter(Propertyleases.property_id==pid).filter(Propertyleases.lease_payment_flag=='Pending').filter(Propertyleases.lease_cancel==0).group_by(Usercode.user_id,Propertyleases.property_id, Propertyleases.lease_code, Propertyleases.floorsno, Propertyleases.aptno).all()

    RentAptDataMsg = []
    START = 0
    for idx, item in enumerate(items[START:], START):
        new_link = {
            'msg_type': 'Management',
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'lease_code': item.lease_code,
            'senduser_id': item.user_id,
            #'id': idx,
        }
        RentAptDataMsg.append(new_link)

    return jsonify({
        'RentAptData': RentAptData,
        'cntRentAptData': cntRentAptData,
        'RentAptDataMsg': RentAptDataMsg,
    }), HTTP_200_OK

@propertyleases.put("/PendLeaseData")
@jwt_required()
def get_PendLeaseData():
    Pid = request.json.get('GetPID', '')
    FNo = request.json.get('GetFloorNo', '')
    ANo = request.json.get('GetAptNo', '')
    RentID = request.json.get('GetRentID', '')
    current_user = int(request.json.get('UserID', ''))

    if RentID == 0:
        LeaseData = Propertyleases.query.filter_by(property_id=Pid, floorsno=FNo, aptno=ANo, lease_payment_flag='Pending').first()
    else:
        LeaseData = Propertyleases.query.filter_by(id=RentID, lease_payment_flag='Pending').first()
    
    #user_id=current_user, 

    if not LeaseData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    """ PendLeaseData = []
    START = 0
    for idx, item in enumerate(LeaseData[START:], START):
        new_link = {
            'lease_id': item.id,
            'pid': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
        }
        PendLeaseData.append(new_link) """

    return jsonify({
        'lease_id': LeaseData.id,
        'pid': LeaseData.property_id,
        'floorsno': LeaseData.floorsno,
        'aptno': LeaseData.aptno,
        'lease_code': LeaseData.lease_code,
        'lease_price': LeaseData.lease_price,
        'lease_start_date': LeaseData.lease_start_date,
        'lease_end_date': LeaseData.lease_end_date,
        'lease_price_pay': LeaseData.lease_price_pay,
    }), HTTP_200_OK

@propertyleases.put('/updatepay/<int:id>/<int:user_id>')
@propertyleases.patch('/updatepay/<int:id>/<int:user_id>')
@jwt_required()
def editupdatepay(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    propertyleases = Propertyleases.query.filter_by(id=id).first()
    #user_id=current_user,

    if not propertyleases:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    lease_price_pay = request.get_json().get('lease_price_pay', '')

    sYear = request.get_json().get('sYear', '')
    sMonth = request.get_json().get('sMonth', '')
    sDate = request.get_json().get('sDate', '') 

    lease_payment_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))

    propertyleases.lease_price_pay = lease_price_pay
    propertyleases.lease_payment_date = lease_payment_date
    propertyleases.lease_payment_flag = 'Paid'
    propertyleases.lease_stripe_charge_id = ''

    db.session.commit()

    return jsonify({
        'id': propertyleases.id,
        'user_id': propertyleases.user_id,
        'property_id': propertyleases.property_id,
        'floorsno': propertyleases.floorsno,
        'aptno': propertyleases.aptno,
    }), HTTP_200_OK

@propertyleases.put("/allrentdatabyaptno")
@jwt_required()
def get_allrentdata():
    pid = request.json.get('PropID', '')
    aptno = request.json.get('AptNo', '')
    current_user = int(request.json.get('UserID', ''))

    currDt = date.today()

    OverdueData = []
    LeasesData = db.session.query(Propertyleases).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_payment_date == None).filter(Propertyleases.lease_end_date < currDt).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        if (item.lease_payment_date):
            pay_date = (datetime.datetime.strptime(str(item.lease_payment_date), "%Y-%m-%d")).strftime('%b. %d %Y')
        else:
            pay_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d") + timedelta(days=1)).strftime('%b. %d %Y')

        if (item.lease_end_date):
            end_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        if (item.lease_start_date):
            start_date = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_price_pay': item.lease_price_pay,
            'lease_payment_flag': item.lease_payment_flag,
            'lease_payment_date': item.lease_payment_date,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'pay_date': pay_date,
            'end_date': end_date,
            'start_date': start_date,
        }

        OverdueData.append(new_link)

    PastData = []
    LeasesData = db.session.query(Propertyleases).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_payment_date != None).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        if (item.lease_payment_date):
            pay_date = (datetime.datetime.strptime(str(item.lease_payment_date), "%Y-%m-%d")).strftime('%b. %d %Y')
        else:
            pay_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d") + timedelta(days=1)).strftime('%b. %d %Y')

        if (item.lease_end_date):
            end_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        if (item.lease_start_date):
            start_date = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_price_pay': item.lease_price_pay,
            'lease_payment_flag': item.lease_payment_flag,
            'lease_payment_date': item.lease_payment_date,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'pay_date': pay_date,
            'end_date': end_date,
            'start_date': start_date,
        }

        PastData.append(new_link)

    UpcomingData = []
    LeasesData = db.session.query(Propertyleases).filter(Propertyleases.property_id == pid).filter(Propertyleases.aptno == aptno).filter(Propertyleases.lease_payment_date == None).filter(Propertyleases.lease_end_date >= currDt).all()
    #.filter(Propertyleases.user_id == current_user)
    for item in LeasesData:
        if (item.lease_payment_date):
            pay_date = (datetime.datetime.strptime(str(item.lease_payment_date), "%Y-%m-%d")).strftime('%b. %d %Y')
        else:
            pay_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d") + timedelta(days=1)).strftime('%b. %d %Y')

        if (item.lease_end_date):
            end_date = (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        if (item.lease_start_date):
            start_date = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y')

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_price_pay': item.lease_price_pay,
            'lease_payment_flag': item.lease_payment_flag,
            'lease_payment_date': item.lease_payment_date,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'pay_date': pay_date,
            'end_date': end_date,
            'start_date': start_date,
        }

        UpcomingData.append(new_link)

    return jsonify({
        'OverdueData': OverdueData,
        'UpcomingData': UpcomingData,
        'PastData': PastData,
    }), HTTP_200_OK

@propertyleases.put("/duePaymentbyLeaseCode")
@jwt_required()
def getDuePaymentbyLeaseCode():
    leaseCode = request.json.get('LeaseCode', '')
    lcodeList = (leaseCode, 'None');
    
    userid = request.json.get('UserID', '')

    DisableLeaseCode = []
    LeaseData = db.session.query(Usercode).filter(Usercode.user_id == userid, Usercode.user_active_code == '0').all()
    for item in LeaseData:
        DisableLeaseCode.append(item.user_code) 


    #items = Propertyleases.query.filter_by(lease_code=leaseCode, lease_payment_flag='Pending').order_by(Propertyleases.lease_start_date).all()
    items = db.session.query(Propertyleases).filter(Propertyleases.lease_code.in_(lcodeList)).filter(Propertyleases.lease_payment_flag=='Pending').order_by(Propertyleases.lease_start_date).all()

    YearTextArr = []
    YearText = ''
    START = 0
    for idx1, item1 in enumerate(items[START:], START):
        addYearText = (datetime.datetime.strptime(str(item1.lease_start_date), "%Y-%m-%d")).strftime('%Y')
        if (YearText!=addYearText):
            YearText = addYearText 
            PayRentbyLeaseCode = []
            for item in items:
                if (addYearText==(datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y')):

                    stDate = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d"))
                    delta = relativedelta.relativedelta(stDate, datetime.datetime.now())
                    diff_months = delta.months + (delta.years * 12)

                    new_link = {
                        'id': item.id,
                        'lease_code': item.lease_code,
                        'lease_price': item.lease_price,
                        'diff_months': diff_months,
                        'lease_start_date': item.lease_start_date,
                        'lease_end_date': item.lease_end_date,
                        'leaseStartYear': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y'),
                        'leaseStartMonthName': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B'),
                        'lease_start_date_text': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%d/%m/%Y'),
                    }
                    PayRentbyLeaseCode.append(new_link)
            
            new_year = {
                'YearText': addYearText,
                'YearData': PayRentbyLeaseCode,
            }
            YearTextArr.append(new_year)



    items = db.session.query(Propertyleases).filter(Propertyleases.lease_code.in_(DisableLeaseCode)).filter(Propertyleases.lease_payment_flag=='Pending').order_by(Propertyleases.lease_start_date).all()

    DisableData = []
    YearText = ''
    START = 0
    for idx1, item1 in enumerate(items[START:], START):
        addYearText = (datetime.datetime.strptime(str(item1.lease_start_date), "%Y-%m-%d")).strftime('%Y')
        if (YearText!=addYearText):
            YearText = addYearText 
            PayRentbyLeaseCode = []
            for item in items:
                if (addYearText==(datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y')):
                    stDate = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d"))
                    delta = relativedelta.relativedelta(stDate, datetime.datetime.now())
                    diff_months = delta.months + (delta.years * 12)

                    new_link = {
                        'id': item.id,
                        'lease_code': item.lease_code,
                        'lease_price': item.lease_price,
                        'diff_months': diff_months,
                        'lease_start_date': item.lease_start_date,
                        'lease_end_date': item.lease_end_date,
                        'leaseStartYear': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y'),
                        'leaseStartMonthName': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B'),
                        'lease_start_date_text': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%d/%m/%Y'),
                    }
                    PayRentbyLeaseCode.append(new_link)
            
            new_year = {
                'YearText': addYearText,
                'YearData': PayRentbyLeaseCode,
            }
            DisableData.append(new_year)

    return jsonify({
        'YearTextArr': YearTextArr,
        'DisableLeaseCode': DisableLeaseCode,
        'DisableData': DisableData
    }), HTTP_200_OK

@propertyleases.put('/quickpay/<int:id>')
@propertyleases.patch('/quickpay/<int:id>')
@jwt_required()
def quickpay(id):
    propertyleases = Propertyleases.query.filter_by(id=id).first()
    if not propertyleases:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    propData = db.session.query(Propertydata).with_entities(Propertydata.id, Propertydata.user_id, User.stripe_api_key, User.comm_percentage).join(Propertyleases, Propertyleases.property_id == Propertydata.id).join(User, User.id == Propertydata.user_id).filter(Propertyleases.id==id).first()
    stripe_api_key = propData.stripe_api_key

    if (stripe_api_key==''):
        return jsonify({'payment': 'error', 'message': 'Management Stripe Account not created' }), HTTP_200_OK
    
    stripe.api_key = stripe_api_key

    cvcNo = request.get_json().get('cvcNo', '')

    amount = request.get_json().get('lease_price_pay', '') * 100
    paymentDoneFlag = False

    if (request.get_json().get('quick_pay', '')):
        userPayData = Userpaymethod.query.filter_by(user_id=request.get_json().get('pay_user_id', ''), default_method='1').first()
    else:
        userPayData = Userpaymethod.query.filter_by(user_id=request.get_json().get('pay_user_id', ''), id=request.get_json().get('selectedCardID', '')).first()

    f = Fernet(userPayData.fernet_key)
    cardDataTok = f.decrypt(userPayData.stripe_token_data).decode()
    cardData = json.loads(cardDataTok)

    ChargeDataID = ''
    TokenInfo = stripe.Token.create(
        card={
            "number": cardData['number'],
            "exp_month": cardData['exp_month'],
            "exp_year": cardData['exp_year'],
            "cvc": cvcNo, #cardData['cvc'],
        },
    )

    try:
        ChargeData = stripe.Charge.create(
            amount=amount,
            currency="usd",
            source=TokenInfo['id'],
            description=request.get_json().get('pay_description', ''),
        )

        ChargeDataID = ChargeData.id

    except stripe.error.InvalidRequestError:
        ChargeDataID = ''

    if (ChargeDataID!=''):
        lease_price_pay = request.get_json().get('lease_price_pay', '')

        sYear = request.get_json().get('sYear', '')
        sMonth = request.get_json().get('sMonth', '')
        sDate = request.get_json().get('sDate', '') 

        lease_payment_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))

        propertyleases.lease_price_pay = lease_price_pay
        propertyleases.lease_payment_date = lease_payment_date
        propertyleases.lease_payment_flag = 'Paid'
        propertyleases.lease_stripe_charge_id = ChargeDataID

        db.session.commit()
        paymentDoneFlag = True
    else:
        paymentDoneFlag = False

    if (paymentDoneFlag):
        return jsonify({
            'id': propertyleases.id,
            'lease_payment_flag': propertyleases.lease_payment_flag,
            'lease_payment_date': propertyleases.lease_payment_date,
            'lease_price_pay': propertyleases.lease_price_pay,
            'payment': 'success',
        }), HTTP_200_OK
    else:
        return jsonify({
            'id': propertyleases.id,
            'payment': 'error',
            'message': 'Payment Not Done'
        }), HTTP_200_OK

@propertyleases.put("/paidbyLeaseCode")
@jwt_required()
def getPaidbyLeaseCode():
    leaseCode = request.json.get('LeaseCode', '')
    lcodeList = (leaseCode, 'None');
    
    userid = request.json.get('UserID', '')

    DisableLeaseCode = []
    LeaseData = db.session.query(Usercode).filter(Usercode.user_id == userid, Usercode.user_active_code == '0').all()
    for item in LeaseData:
        DisableLeaseCode.append(item.user_code) 




    #items = Propertyleases.query.filter_by(lease_code=leaseCode, lease_payment_flag='Paid').order_by(Propertyleases.lease_start_date).all()
    items = db.session.query(Propertyleases).filter(Propertyleases.lease_code.in_(lcodeList)).filter(Propertyleases.lease_payment_flag=='Paid').order_by(Propertyleases.lease_start_date).all()

    YearTextArr = []
    YearText = ''
    START = 0
    for idx1, item1 in enumerate(items[START:], START):
        addYearText = (datetime.datetime.strptime(str(item1.lease_start_date), "%Y-%m-%d")).strftime('%Y')
        if (YearText!=addYearText):
            YearText = addYearText 
            PayRentbyLeaseCode = []
            for item in items:
                if (addYearText==(datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y')):
                    new_link = {
                        'id': item.id,
                        'lease_code': item.lease_code,
                        'lease_price': item.lease_price,
                        'lease_start_date': item.lease_start_date,
                        'lease_end_date': item.lease_end_date,
                        'leaseStartYear': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y'),
                        'leaseStartMonthName': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B'),
                        'lease_start_date_text': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%d/%m/%Y'),
                    }
                    PayRentbyLeaseCode.append(new_link)
            
            new_year = {
                'YearText': addYearText,
                'YearData': PayRentbyLeaseCode,
            }
            YearTextArr.append(new_year)



    items = db.session.query(Propertyleases).filter(Propertyleases.lease_code.in_(DisableLeaseCode)).filter(Propertyleases.lease_payment_flag=='Paid').order_by(Propertyleases.lease_start_date).all()

    DisableData = []
    YearText = ''
    START = 0
    for idx1, item1 in enumerate(items[START:], START):
        addYearText = (datetime.datetime.strptime(str(item1.lease_start_date), "%Y-%m-%d")).strftime('%Y')
        if (YearText!=addYearText):
            YearText = addYearText 
            PayRentbyLeaseCode = []
            for item in items:
                if (addYearText==(datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y')):
                    new_link = {
                        'id': item.id,
                        'lease_code': item.lease_code,
                        'lease_price': item.lease_price,
                        'lease_start_date': item.lease_start_date,
                        'lease_end_date': item.lease_end_date,
                        'leaseStartYear': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%Y'),
                        'leaseStartMonthName': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B'),
                        'lease_start_date_text': (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%d/%m/%Y'),
                    }
                    PayRentbyLeaseCode.append(new_link)
            
            new_year = {
                'YearText': addYearText,
                'YearData': PayRentbyLeaseCode,
            }
            DisableData.append(new_year)


    return jsonify({
        'YearTextArr': YearTextArr,
        'DisableLeaseCode': DisableLeaseCode,
        'DisableData': DisableData
    }), HTTP_200_OK

@propertyleases.put("/alldatabyleasecode")
@jwt_required()
def get_alldatabyleasecode():
    lcode = request.json.get('LeaseCode', '')
    userid = request.json.get('UserID', '')

    #lcodeList = (lcode,'10VQHV8YVHLD', '137DLWCG91PT');
    lcodeList = (lcode, 'None');

    DisableLeaseCode = []
    LeaseData = db.session.query(Usercode).filter(Usercode.user_id == userid, Usercode.user_active_code == '0').all()
    for item in LeaseData:
        DisableLeaseCode.append(item.user_code)


    currDt = date.today()

    columns = [Propertydata.address, City.name.label('cityname'), State.name.label('statename'), Country.name.label('countryname'), Propertyleases.id, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.file_storagekey, Propertyleases.lease_code, Propertyleases.lease_price, Propertyleases.lease_start_date, Propertyleases.lease_end_date]





    DisableData = []
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).join(Propertydata, Propertyleases.property_id == Propertydata.id).join(City, Propertydata.city_id == City.id).join(State, Propertydata.state_id == State.id).join(Country, Propertydata.country_id == Country.id).filter(Propertyleases.lease_code.in_(DisableLeaseCode)).all()
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        FData = []
        FileData = db.session.query(Propertyleasesfile).filter(Propertyleasesfile.leases_id == item.id).all()
        for subitem in FileData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)
        

        RentPeriod = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y') + " - " + (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'address': item.address,
            'cityname': item.cityname,
            'statename': item.statename,
            'countryname': item.countryname,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'RentPeriod': RentPeriod,
            'TenantsData': TData,
            'FileData': FData,
        }

        DisableData.append(new_link)







    CurrentData = []
    #LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.lease_code == lcode).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).all()
    #LeasesData = db.session.query(Propertyleases.lease_price, Propertyleases.file_storagekey, Propertydata.address.label('address'), Propertyleases.id.label('Propertyleasesid'), Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.lease_code, Propertyleases.aptno, Propertyleases.lease_start_date, Propertyleases.lease_end_date).join(Propertydata, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==lcode).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).all()
    #LeasesData = db.session.query(Propertyleases).join(Propertydata, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==lcode).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).all()
    
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).join(Propertydata, Propertyleases.property_id == Propertydata.id).join(City, Propertydata.city_id == City.id).join(State, Propertydata.state_id == State.id).join(Country, Propertydata.country_id == Country.id).filter(Propertyleases.lease_code.in_(lcodeList)).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).all()
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        FData = []
        FileData = db.session.query(Propertyleasesfile).filter(Propertyleasesfile.leases_id == item.id).all()
        for subitem in FileData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)
        

        RentPeriod = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y') + " - " + (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'address': item.address,
            'cityname': item.cityname,
            'statename': item.statename,
            'countryname': item.countryname,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'RentPeriod': RentPeriod,
            'TenantsData': TData,
            'FileData': FData,
        }

        CurrentData.append(new_link)

    PastData = []
    #LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.lease_code == lcode).filter(Propertyleases.lease_start_date < currDt).filter(Propertyleases.lease_end_date < currDt).all()
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).join(Propertydata, Propertyleases.property_id == Propertydata.id).join(City, Propertydata.city_id == City.id).join(State, Propertydata.state_id == State.id).join(Country, Propertydata.country_id == Country.id).filter(Propertyleases.lease_code.in_(lcodeList)).filter(Propertyleases.lease_start_date < currDt).filter(Propertyleases.lease_end_date < currDt).all()
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        FData = []
        FileData = db.session.query(Propertyleasesfile).filter(Propertyleasesfile.leases_id == item.id).all()
        for subitem in FileData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)

        RentPeriod = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y') + " - " + (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'address': item.address,
            'cityname': item.cityname,
            'statename': item.statename,
            'countryname': item.countryname,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'RentPeriod': RentPeriod,
            'TenantsData': TData,
            'FileData': FData,
        }

        PastData.append(new_link)

    UpcomingData = []
    #LeasesData = db.session.query(Propertyleases).with_entities(*columns).filter(Propertyleases.lease_code == lcode).filter(Propertyleases.lease_start_date > currDt).filter(Propertyleases.lease_end_date > currDt).all()
    LeasesData = db.session.query(Propertyleases).with_entities(*columns).join(Propertydata, Propertyleases.property_id == Propertydata.id).join(City, Propertydata.city_id == City.id).join(State, Propertydata.state_id == State.id).join(Country, Propertydata.country_id == Country.id).filter(Propertyleases.lease_code.in_(lcodeList)).filter(Propertyleases.lease_start_date > currDt).filter(Propertyleases.lease_end_date > currDt).all()
    for item in LeasesData:
        TData = []
        TenantsData = db.session.query(Propertyleasestenant).with_entities(Propertyleasestenant.tenant_fname, Propertyleasestenant.tenant_lname).filter(Propertyleasestenant.leases_id == item.id).all()
        for subitem in TenantsData:
            new_add = {
                'tenant_fname': subitem.tenant_fname,
                'tenant_lname': subitem.tenant_lname,
            }
            TData.append(new_add)

        FData = []
        FileData = db.session.query(Propertyleasesfile).filter(Propertyleasesfile.leases_id == item.id).all()
        for subitem in FileData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)

        RentPeriod = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d %Y') + " - " + (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');

        new_link = {
            'id': item.id,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'address': item.address,
            'cityname': item.cityname,
            'statename': item.statename,
            'countryname': item.countryname,
            'file_storagekey': item.file_storagekey,
            'lease_code': item.lease_code,
            'lease_price': item.lease_price,
            'lease_start_date': item.lease_start_date,
            'lease_end_date': item.lease_end_date,
            'RentPeriod': RentPeriod,
            'TenantsData': TData,
            'FileData': FData,
        }

        UpcomingData.append(new_link)

    AddNewLease = 0
    if (len(CurrentData) == 0 and len(UpcomingData) == 0):
        AddNewLease = 1

    cntData = Propertyleases.query.filter(Propertyleases.lease_code == lcode).filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).filter(Propertyleases.lease_cancel==1).count()
    if (len(CurrentData) == cntData):
        AddNewLease = 1

    MaxData = db.session.query(Propertyleases).with_entities(func.max(Propertyleases.lease_end_date), extract('month', Propertyleases.lease_end_date).label("month_no"), extract('day', Propertyleases.lease_end_date).label("day_no"), extract('year', Propertyleases.lease_end_date).label("year_no"), Propertyleases.lease_propose_renewal, Propertyleases.id, Propertyleases.file_storagekey).filter(Propertyleases.lease_code == lcode).first()

    MaxDateText = ''
    MaxDate = ''
    MaxID = 0
    MaxStorageKey = ''
    ProposeRenewalFlag = 0
    if MaxData[0]:
        #MaxDateText = datetime.datetime(1, int(MaxData[1]), 1).strftime("%B") + ' ' + str(MaxData[2]) + ' ' + str(MaxData[3])
        MaxDateText = (datetime.datetime.strptime(str(MaxData[0]), "%Y-%m-%d") + timedelta(days=1)).strftime('%b %d %Y')

        MaxDate = MaxData[0]
        ProposeRenewalFlag = MaxData[4]
        MaxID = MaxData[5]
        MaxStorageKey = MaxData[6]
    
    RenewalNewLease = 0
    if (ProposeRenewalFlag==1):
        RenewalNewLease = 1
           
    return jsonify({
        'AddNewLease': AddNewLease,
        'RenewalNewLease': RenewalNewLease,
        'MaxID': MaxID,
        'MaxStorageKey': MaxStorageKey,
        'MaxDate': MaxDate,
        'MaxDateText': MaxDateText,
        'CurrentData': CurrentData,
        'UpcomingData': UpcomingData,
        'PastData': PastData,
        'DisableData': DisableData,
        'DisableLeaseCode' : DisableLeaseCode,
    }), HTTP_200_OK

@propertyleases.put("/leasedata")
@jwt_required()
def get_propleasedata():
    MainUserID = request.json.get('MainUserID', '')
    UserID = request.json.get('UserID', '')

    #itemData = Propertyleases.query.filter_by(user_id=MainUserID).all()
    items = db.session.query(Usercode.user_prop_name, Usercode.user_prop_suffix, Propertyleases.lease_payment_flag, Propertyleases.lease_price, Propertyleases.lease_start_date, Propertyleases.lease_end_date, Propertyleases.floorsno, Propertyleases.aptno).join(Usercode, Propertyleases.lease_code == Usercode.user_code).join(Propertydata, Propertydata.id == Propertyleases.property_id).filter(Usercode.user_id==UserID).filter(Propertydata.user_id==MainUserID).all()

    Retdata = []
    for item in items:
        RentPeriod = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%b. %d') + " - " + (datetime.datetime.strptime(str(item.lease_end_date), "%Y-%m-%d")).strftime('%b. %d %Y');
        Retdata.append({
            "PropName" : item.user_prop_name + "["+ item.user_prop_suffix +"]", 
            "lease_price" : item.lease_price, 
            "lease_priod" : RentPeriod, 
            "lease_payment_flag" : item.lease_payment_flag, 
            "floorsno" : item.floorsno, 
            "aptno" : item.aptno
        })

    return jsonify({
        'Retdata': Retdata,
    }), HTTP_200_OK

@propertyleases.get("/sendnotification")
@jwt_required()
def get_sendnotification():

    LeasesData = db.session.query(Propertyleases).with_entities(Propertyleases.lease_notification_send, Propertyleases.lease_code, Propertyleases.id, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, func.max(Propertyleases.lease_end_date).label("end_date"), Propertydata.user_id.label("m_user_id"), Propertydata.name.label("prop_name"), Propertydata.suffix.label("prop_suffix")).join(Propertydata, Propertydata.id == Propertyleases.property_id).filter(Propertyleases.lease_cancel==0).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.lease_code).all()
    for item in LeasesData:
        if (item.lease_notification_send!=1) :
            end_date_text = (datetime.datetime.strptime(str(item.end_date), "%Y-%m-%d")).strftime('%b. %d %Y')
            prop_text = item.prop_name + '[' + item.prop_suffix + '] [' + str(item.aptno) + '] [' + str(item.floorsno) + ']' + item.lease_code

            LeaseRenewalAddNotification(item.m_user_id, end_date_text, item.lease_code, prop_text, item.property_id)

            noData = Propertyleases.query.filter_by(id=item.id).first()
            noData.lease_notification_send = '1'
            db.session.commit()

    currDt = date.today()

    RentData = db.session.query(Propertyleases).filter(Propertyleases.lease_cancel==0).filter(Propertyleases.lease_payment_flag=='Pending').filter(Propertyleases.rent_notification_send!='1').filter(Propertyleases.lease_end_date >= currDt).all()
    for item in RentData:
        start_date_text = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B, %Y')

        n_type = 'Normal'
        n_subtitle = 'Reminder'
        n_desc = 'Payment for the month of <b>'+ start_date_text +'</b> is due soon. Tap here to see details.'
        
        RentAddNotification(n_type, 'Rent', n_subtitle, n_desc, item.lease_code, item.property_id)

        noData = Propertyleases.query.filter_by(id=item.id).first()
        noData.rent_notification_send = '1'
        db.session.commit()

    RentData = db.session.query(Propertyleases).filter(Propertyleases.lease_cancel==0).filter(Propertyleases.lease_payment_flag=='Pending').filter(Propertyleases.rent_notification_send!='1').filter(Propertyleases.lease_end_date < currDt).all()
    for item in RentData:
        start_date_text = (datetime.datetime.strptime(str(item.lease_start_date), "%Y-%m-%d")).strftime('%B, %Y')

        n_type = 'Urgent'
        n_subtitle = 'Overdue'
        n_desc = 'Payment for the month of <b>'+ start_date_text +'</b> is overdue. Tap here to pay now.'
        
        RentAddNotification(n_type, 'Rent', n_subtitle, n_desc, item.lease_code, item.property_id)

        noData = Propertyleases.query.filter_by(id=item.id).first()
        noData.rent_notification_send = '1'
        db.session.commit()


    return jsonify({
        'SendData': True,
    }), HTTP_200_OK

def RentAddNotification(n_type, n_title, n_subtitle, n_desc, LCode, PropID):

    leaseCodeData = Usercode.query.filter_by(user_code=LCode).all()
    for item in leaseCodeData:
        noto_add = Notification(
                        user_id=item.user_id, 
                        n_type=n_type, 
                        n_date=date.today(), 
                        n_property_id=PropID, 
                        n_lease_code=LCode, 
                        n_title=n_title, 
                        n_subtitle=n_subtitle, 
                        n_desc=n_desc, 
                        n_link="/pay-rents", 
                        n_read=0, 
                        n_clear=0)
        db.session.add(noto_add)
        db.session.commit()

def LeaseRenewalAddNotification(MUserID, EndDate, LCode, PropText, PropID):

    noto_add = Notification(
                    user_id=MUserID, 
                    n_type='Urgent', 
                    n_date=date.today(), 
                    n_property_id=PropID, 
                    n_lease_code=LCode, 
                    n_title='Lease', 
                    n_subtitle='Renewal', 
                    #n_desc='Your Property Detail <b>'+ PropText +'</b>. Your lease ends <b>'+ EndDate +'</b>. Tap here to see details.', 
                    n_desc='Your lease <b>"'+ LCode +'"</b> ends <b>'+ EndDate +'</b>. Tap here to see details.', 
                    n_link="/manage-tenants", 
                    n_read=0, 
                    n_clear=0)
    db.session.add(noto_add)
    db.session.commit()

    leaseCodeData = Usercode.query.filter_by(user_code=LCode).all()
    for item in leaseCodeData:
        noto_add = Notification(
                        user_id=item.user_id, 
                        n_type='Urgent', 
                        n_date=date.today(), 
                        n_property_id=PropID, 
                        n_lease_code=LCode, 
                        n_title='Lease', 
                        n_subtitle='Renewal', 
                        #n_desc='Your Property Detail <b>'+ PropText +'</b>. Your lease ends <b>'+ EndDate +'</b>. Tap here to see details.', 
                        n_desc='Your lease <b>"'+ LCode +'"</b> ends <b>'+ EndDate +'</b>. Tap here to see details.', 
                        n_link="/manage-leases", 
                        n_read=0, 
                        n_clear=0)
        db.session.add(noto_add)
        db.session.commit()

    #leaseCodeData = Usercode.query.filter_by(user_active_code='1', user_code=LCode).all()
    #for item in leaseCodeData:
    #    noto_add = Notification(user_id=item.user_id, n_type='Urgent', n_date=date.today(), n_title='New', n_subtitle='Tenant', n_desc='A new tenant, <b>'+ UserName +'</b> has joined your lease. Tap here to see the lease details.', n_link="/manage-leases", n_read=0, n_clear=0)
    #    db.session.add(noto_add)
    #    db.session.commit()

@propertyleases.put("/duePaymentData")
@jwt_required()
def getduePaymentData():

    userid = request.json.get('UserID', '')
    items = db.session.query(Billinghistory).filter(Billinghistory.billing_user_id==userid).filter(Billinghistory.billing_payment_flag=='Pending').order_by(Billinghistory.id.desc()).all()

    UnPaidData = []
    for item in items:
        new_link = {
            'id': item.id,
            'billing_date_text': item.billing_date_text,
            'billing_cost': int(item.billing_cost + 0.5), #round(item.billing_cost),
            'billing_lease_cnt': item.billing_lease_cnt,
            'billing_status': item.billing_status,
            'billing_payment_flag': item.billing_payment_flag,
        }
        UnPaidData.append(new_link)

    return jsonify({
        'UnPaidData': UnPaidData,
    }), HTTP_200_OK

@propertyleases.put('/billpay/<int:id>')
@propertyleases.patch('/billpay/<int:id>')
@jwt_required()
def billpay(id):
    updateQuery = Billinghistory.query.filter_by(id=id).first()
    if not updateQuery:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    stripe_api_key = ''
    SiteConfigsData = Siteconfigs.query.filter(Siteconfigs.site_text=='site_stripe_api_key').first()
    if (SiteConfigsData.site_value):
        stripe_api_key = SiteConfigsData.site_value

    if (stripe_api_key==''):
        return jsonify({'payment': 'error', 'message': 'Site Stripe Account not created' }), HTTP_200_OK
    
    stripe.api_key = stripe_api_key

    cvcNo = request.get_json().get('cvcNo', '')
    amount = request.get_json().get('amount_pay', '') * 100
    paymentDoneFlag = False

    if (request.get_json().get('quick_pay', '')):
        userPayData = Userpaymethod.query.filter_by(user_id=request.get_json().get('pay_user_id', ''), default_method='1').first()
    else:
        userPayData = Userpaymethod.query.filter_by(user_id=request.get_json().get('pay_user_id', ''), id=request.get_json().get('selectedCardID', '')).first()

    f = Fernet(userPayData.fernet_key)
    cardDataTok = f.decrypt(userPayData.stripe_token_data).decode()
    cardData = json.loads(cardDataTok)

    ChargeDataID = ''
    TokenInfo = stripe.Token.create(
        card={
            "number": cardData['number'],
            "exp_month": cardData['exp_month'],
            "exp_year": cardData['exp_year'],
            "cvc": cvcNo,
        },
    )

    try:
        ChargeData = stripe.Charge.create(
            amount=amount,
            currency="usd",
            source=TokenInfo['id'],
            description='',
        )

        ChargeDataID = ChargeData.id

    except stripe.error.InvalidRequestError:
        ChargeDataID = ''

    if (ChargeDataID!=''):
        amount_pay = request.get_json().get('amount_pay', '')

        sYear = request.get_json().get('sYear', '')
        sMonth = request.get_json().get('sMonth', '')
        sDate = request.get_json().get('sDate', '') 

        lease_payment_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))

        updateQuery.billing_payment_amount = amount_pay
        updateQuery.billing_payment_date = lease_payment_date
        updateQuery.billing_payment_flag = 'Paid'
        updateQuery.billing_stripe_charge_id = ChargeDataID

        db.session.commit()
        paymentDoneFlag = True
    else:
        paymentDoneFlag = False

    if (paymentDoneFlag):
        return jsonify({
            'id': updateQuery.id,
            'payment': 'success',
        }), HTTP_200_OK
    else:
        return jsonify({
            'id': updateQuery.id,
            'payment': 'error',
            'ChargeDataID': ChargeDataID,
            'stripe_api_key': stripe_api_key,
            'cvcNo': cvcNo,
            'amount': amount,
            'cardData': cardData,
            'message': 'Payment Not Done'
        }), HTTP_200_OK

@propertyleases.put("/paidPaymentData")
@jwt_required()
def getPaidPaymentData():

    userid = request.json.get('UserID', '')
    items = db.session.query(Billinghistory).filter(Billinghistory.billing_user_id==userid).filter(Billinghistory.billing_payment_flag=='Paid').order_by(Billinghistory.id.desc()).all()

    PaidData = []
    for item in items:
        new_link = {
            'id': item.id,
            'billing_date_text': item.billing_date_text,
            'billing_cost': item.billing_payment_amount,
            'billing_lease_cnt': item.billing_lease_cnt,
            'billing_status': item.billing_status,
            'billing_payment_flag': item.billing_payment_flag,
        }
        PaidData.append(new_link)

    return jsonify({
        'PaidData': PaidData,
    }), HTTP_200_OK

@propertyleases.get("/GetPaymentData/<int:id>")
@jwt_required()
def GetPaymentData(id):
    BillingData = Billinghistory.query.filter_by(id=id).first()

    if not BillingData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    CardNumber = ''
    PaymentMethod = ''
    if ((BillingData.billing_stripe_charge_id!='') and (BillingData.billing_stripe_charge_id is not None)):
        stripe_api_key = ''
        SiteConfigsData = Siteconfigs.query.filter(Siteconfigs.site_text=='site_stripe_api_key').first()
        if (SiteConfigsData.site_value):
            stripe_api_key = SiteConfigsData.site_value

        if (stripe_api_key==''):
            return jsonify({'payment': 'error', 'message': 'Site Stripe Account not created' }), HTTP_200_OK
        
        stripe.api_key = stripe_api_key

        try:
            ChargeData = stripe.Charge.retrieve(
                BillingData.billing_stripe_charge_id,
            )
            
            if (ChargeData.payment_method_details.card.last4):
                CardNumber = "**** **** **** " + ChargeData.payment_method_details.card.last4

            if (ChargeData.payment_method_details.card.brand):
                PaymentMethod = ChargeData.payment_method_details.card.brand

        except stripe.error.InvalidRequestError:
            CardNumber = ''
            PaymentMethod = ''

    if (BillingData.billing_payment_date):
        PaidDate = (datetime.datetime.strptime(str(BillingData.billing_payment_date), "%Y-%m-%d")).strftime('%d/%b/%Y')
    else:
        PaidDate = ''

    return jsonify({
        'id': BillingData.id,
        'billing_date_text': BillingData.billing_date_text,
        'billing_payment_flag': BillingData.billing_payment_flag,
        'billing_cost': int(BillingData.billing_cost + 0.5),
        'billing_payment_amount': BillingData.billing_payment_amount,
        'billing_payment_date':PaidDate,
        'CardNumber': CardNumber,
        'PaymentMethod': PaymentMethod,
    }), HTTP_200_OK

@propertyleases.put('/updatebilldetail')
@jwt_required()
def updatebilldetail():
    LeasesData = db.session.query(Propertyleases).with_entities(Propertyleases.id, Propertyleases.lease_price, Propertydata.user_id, User.comm_percentage, Propertyleases.lease_start_date).join(Propertydata, Propertyleases.property_id == Propertydata.id).join(User, User.id == Propertydata.user_id).filter(Propertyleases.billing_flag_update == '0').all()
    for LeasesDataInfo in LeasesData:
        if (LeasesDataInfo.comm_percentage>0):
            CommPer = LeasesDataInfo.comm_percentage
        else:
            CommPer = float(request.get_json().get('SiteCommPer', ''))

        billing_user_id = LeasesDataInfo.user_id
        #billing_date = LeasesDataInfo.lease_start_date

        ye = int((LeasesDataInfo.lease_start_date).strftime('%Y'))
        mo = int((LeasesDataInfo.lease_start_date).strftime('%m'))
        billing_date = datetime.datetime(ye, mo, 1)

        billing_date_text = (LeasesDataInfo.lease_start_date).strftime('%B, %Y')
        billing_cost = round(((LeasesDataInfo.lease_price * CommPer) / 100),2)

        BillData = Billinghistory.query.filter_by(billing_user_id=billing_user_id, billing_date_text=billing_date_text, billing_status='Continue', billing_payment_flag='Pending').first()
        if BillData:
            billing_id = BillData.id

            BillData.billing_lease_cnt = (BillData.billing_lease_cnt + 1)
            BillData.billing_cost = (BillData.billing_cost + billing_cost)
            db.session.commit()
        else:
            BillinghistoryAdd = Billinghistory(
                        billing_user_id=billing_user_id, 
                        billing_date=billing_date, 
                        billing_date_text=billing_date_text, 
                        billing_cost=billing_cost, 
                        billing_lease_cnt='1', 
                        billing_status='Continue', 
                        billing_payment_flag='Pending', 
                        #billing_payment_date='', 
                        billing_payment_amount='0', 
                        billing_stripe_charge_id=''
                    )
            db.session.add(BillinghistoryAdd)
            db.session.commit()

            billing_id = BillinghistoryAdd.id

        BillingsubAdd = Billinghistorysub(
                    billing_id=billing_id, 
                    billing_leases_id=LeasesDataInfo.id, 
                    billing_lease_price=LeasesDataInfo.lease_price, 
                    billing_comm_per=CommPer, 
                    billing_comm_amt=billing_cost, 
                )
        db.session.add(BillingsubAdd)
        db.session.commit()

        getData = Propertyleases.query.filter_by(id=LeasesDataInfo.id).first()
        getData.billing_flag_update = '1'
        db.session.commit()

    current = datetime.datetime.now() + timedelta(-30)
    curryear = current.year
    currmonth = current.month
    BillInfo = db.session.query(Billinghistory).filter(extract('month', Billinghistory.billing_date) <= currmonth, extract('year', Billinghistory.billing_date) <= curryear).all()
    for BillData in BillInfo:
        getData = Billinghistory.query.filter_by(id=BillData.id).first()
        getData.billing_status = 'Done'
        db.session.commit()

    return jsonify({
        'Update': 'Success',
        'message': ''
    }), HTTP_200_OK


def last_day_of_month(any_day):
    # The day 28 exists in every month. 4 days later, it's always next month
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
    # subtracting the number of the current day brings us back one month
    return next_month - datetime.timedelta(days=next_month.day)