from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import Faq, db, Usertype

faqdata = Blueprint("faqdata", __name__, url_prefix="/api/v1/faqdata")

@faqdata.get("/alldatabyusertype/<int:user_type_id>")
@jwt_required()
def get_alldatabyusertype(user_type_id):

    FaqData = []
    if ((user_type_id==0) or (user_type_id==3)):
        FData = db.session.query(Faq).all()
    else:
        FData = db.session.query(Faq).filter(Faq.usertype_id == user_type_id).all()

    for item in FData:
        new_link = {
            'id': item.id,
            'title': item.faq_title,
            'content': item.faq_message,
        }

        FaqData.append(new_link)
    
    return jsonify({
        'FaqData': FaqData,
    }), HTTP_200_OK

@faqdata.get("/allfaq")
@jwt_required()
def get_allfaq():
    data = []
    items = db.session.query(Faq.id, Faq.faq_title, Usertype.usertype.label('usertypeName')).join(Usertype, Usertype.id == Faq.usertype_id).filter_by().all()
    for item in items:
        data.append({"faq_id" : item.id, "faq_title" : item.faq_title, "usertypeName" : item.usertypeName })

    return jsonify(data), HTTP_200_OK

@faqdata.route('/faq/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_faq():

    if request.method == 'POST':
        faq_title = request.get_json().get('faq_title', '')
        faq_message = request.get_json().get('faq_message', '')
        usertype_id = request.get_json().get('usertype_id', '')

        faqAdd = Faq(faq_title=faq_title, faq_message=faq_message, usertype_id=usertype_id)
        db.session.add(faqAdd)
        db.session.commit()

        return jsonify({
            'id': faqAdd.id,
            'faq_title': faqAdd.faq_title,
            'faq_message': faqAdd.faq_message,
            'usertype_id': faqAdd.usertype_id,
        }), HTTP_201_CREATED

    elif request.method == 'DELETE':
        id = request.get_json().get('id', '')
        faqData = Faq.query.filter_by(id=id).first()

        if not faqData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        db.session.delete(faqData)
        db.session.commit()

        return jsonify({
            'deleteFlag': True,
            'id': id,
        }), HTTP_200_OK

    else:
        id = request.get_json().get('id', '')
        faq_title = request.get_json().get('faq_title', '')
        faq_message = request.get_json().get('faq_message', '')
        usertype_id = request.get_json().get('usertype_id', '')

        faqData = Faq.query.filter_by(id=id).first()

        if not faqData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        faqData.faq_title = faq_title
        faqData.faq_message = faq_message
        faqData.usertype_id = usertype_id
        db.session.commit()

        return jsonify({
            'id': faqData.id,
            'faq_title': faqData.faq_title,
            'faq_message': faqData.faq_message,
            'usertype_id': faqData.usertype_id,
        }), HTTP_201_CREATED

@faqdata.get("/faq/<int:id>")
@jwt_required()
def get_faq(id):
    faqData = Faq.query.filter_by(id=id).first()

    if not faqData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'faqID': faqData.id,
        'usertypeID': faqData.usertype_id,
        'faqTitle': faqData.faq_title,
        'faqMsg': faqData.faq_message,
    }), HTTP_200_OK
