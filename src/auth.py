from os import access
import json
from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT, HTTP_404_NOT_FOUND
from flask import Blueprint, jsonify, app, request
from werkzeug.security import check_password_hash, generate_password_hash
#import validators
from flask_jwt_extended import jwt_required, create_access_token, create_refresh_token, get_jwt_identity, get_jwt
from flasgger import swag_from
from src.database import User, db, Usertype, Propertydata, Propertyleases, Usercode, Notification, Userpaymethod, Accessdata
from sqlalchemy import and_
from sqlalchemy import or_
from datetime import date
import stripe
from cryptography.fernet import Fernet

auth = Blueprint("auth", __name__, url_prefix="/api/v1/auth")

#stripe.api_key = 'sk_test_51KWejvLsLWJIq13O6R1qK9vm18bxKoruKkxs85mNoPWgzcmd6fsueWPzjXkxGBcA7B5AxDwI94RaedGRBhbvXOID00ZQ6jy7cf'

@auth.route('/alluser/', methods=['GET', 'POST'])
@jwt_required()
def handle_alluser():

    if request.method == 'GET':
        UserTypeID = request.get_json().get('UserTypeID', '')
        UserMainID = request.get_json().get('UserMainID', '')

        data = []
        if (UserTypeID=='2'):
            items = db.session.query(User.id, User.phoneno, User.username, User.email, User.firstname, User.comm_percentage, User.lastname, Usertype.usertype.label('UserTypeText')).join(Usertype, Usertype.id == User.usertype_id).filter(User.usertype_id == UserTypeID).all()
        else:
            items = db.session.query(User.id, User.phoneno, User.username, User.email, User.firstname, User.comm_percentage, User.lastname, Usertype.usertype.label('UserTypeText')).join(Usertype, Usertype.id == User.usertype_id).join(Usercode, Usercode.user_id == User.id).join(Propertydata, Usercode.user_prop_id == Propertydata.id).filter(Propertydata.user_id == UserMainID).filter(User.usertype_id == UserTypeID).group_by(User.id).all()
        
        for item in items:
            data.append({"comm_percentage" : item.comm_percentage, "user_id" : item.id, "username" : item.username, "email" : item.email, "phoneno" : item.phoneno, "firstname" : item.firstname, "lastname" : item.lastname, "UserTypeText" : item.UserTypeText })

        return jsonify(data), HTTP_200_OK
    else:
        UserTypeID = request.json['UserTypeID']
        UserMainID = request.json['UserMainID']

        data = []
        if (UserTypeID=='2'):
            items = db.session.query(User.id, User.phoneno, User.username, User.email, User.firstname, User.comm_percentage, User.lastname, Usertype.usertype.label('UserTypeText')).join(Usertype, Usertype.id == User.usertype_id).filter(User.usertype_id == UserTypeID).all()
        else:
            items = db.session.query(User.id, User.phoneno, User.username, User.email, User.firstname, User.comm_percentage, User.lastname, Usertype.usertype.label('UserTypeText')).join(Usertype, Usertype.id == User.usertype_id).join(Usercode, Usercode.user_id == User.id).join(Propertydata, Usercode.user_prop_id == Propertydata.id).filter(Propertydata.user_id == UserMainID).filter(User.usertype_id == UserTypeID).group_by(User.id).all()
        
        for item in items:
            data.append({"comm_percentage" : item.comm_percentage, "user_id" : item.id, "username" : item.username, "email" : item.email, "phoneno" : item.phoneno, "firstname" : item.firstname, "lastname" : item.lastname, "UserTypeText" : item.UserTypeText })

        return jsonify(data), HTTP_200_OK

    """ else:
        id = request.get_json().get('id', '')
        name = request.get_json().get('name', '')
        state_id = request.get_json().get('state_id', '')

        cityData = City.query.filter_by(id=id).first()

        if not cityData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        cityData.name = name
        cityData.state_id = state_id
        db.session.commit()

        return jsonify({
            'id': cityData.id,
            'name': cityData.name,
            'state_id': cityData.state_id,
            'created_at': cityData.created_at,
            'updated_at': cityData.updated_at,
        }), HTTP_201_CREATED """

@auth.post('/register')
@jwt_required()
@swag_from('./docs/auth/register.yaml')
def register():
    usertype_id = request.json['usertype']
    username = request.json['email']
    email = request.json['email']
    password = request.json['password']
    firstname = request.json['firstname']
    lastname = request.json['lastname']
    phoneno = request.json['phoneno']

    lease_code = ''
    if (request.json['lease_code']) :
        lease_code = request.json['lease_code']

    access_code = ''
    if (request.json['access_code']) :
        access_code = request.json['access_code']

    prop_id = ''
    prop_name = ''
    prop_suffix = ''
    prop_UserID = ''
    if (lease_code!='') :
        #Tenant
        property = db.session.query(Propertydata).join(Propertyleases, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==lease_code).first()
        if property:
            prop_id = property.id
            prop_name = property.name
            prop_suffix = property.suffix
            prop_UserID = property.user_id
        else:
            return jsonify({'returnFlag':'error', 'message': 'Lease Code not valid'}), HTTP_200_OK



    if (access_code!=''):
        FAccessData = Accessdata.query.filter_by(access_code=access_code, assign_flag='0', active_flag='1').first()
        if not FAccessData:
            return jsonify({'returnFlag':'error', 'message': 'Access Code not valid'}), HTTP_200_OK


    if len(password) < 6:
        return jsonify({'error': "Password is too short"}), HTTP_400_BAD_REQUEST

    if len(username) < 3:
        return jsonify({'error': "User is too short"}), HTTP_400_BAD_REQUEST

    if len(firstname) < 1:
        return jsonify({'error': "First Name is Required"}), HTTP_400_BAD_REQUEST

    if len(lastname) < 1:
        return jsonify({'error': "Last Name is Required"}), HTTP_400_BAD_REQUEST

    if not phoneno.isnumeric() or " " in phoneno:
        return jsonify({'error': "Phone No should be number, also no spaces"}), HTTP_400_BAD_REQUEST

    """ if not validators.email(email):
        return jsonify({'error': "Email is not valid"}), HTTP_400_BAD_REQUEST
 """
    if User.query.filter_by(email=email).first() is not None:
        return jsonify({'error': "Email is taken"}), HTTP_409_CONFLICT

    pwd_hash = generate_password_hash(password)

    stripe_api_key = ''

    #if (usertype_id=='3'):
    #    user = User(usertype_id=usertype_id, user_active='1', username=username, password=pwd_hash, email=email, firstname=firstname, lastname=lastname, phoneno=phoneno, reset_code='', stripe_api_key=stripe_api_key, comm_percentage='0')
    
    user = User(usertype_id=usertype_id, user_active='1', username=username, password=pwd_hash, email=email, firstname=firstname, lastname=lastname, phoneno=phoneno, reset_code='', stripe_api_key=stripe_api_key, comm_percentage='0')
    db.session.add(user)
    db.session.commit()

    if (lease_code!=''):
        TenantAddNotification(prop_UserID, lease_code, firstname + ' ' + lastname, prop_id)

        leaseCode = Usercode.query.filter_by(user_id=user.id, user_active_code='1').first()
        if leaseCode:
            leaseCode.user_active_code = '0'
            db.session.commit()

        userLease = Usercode(user_id=user.id, user_code=lease_code, user_prop_id=prop_id, user_prop_name=prop_name, user_prop_suffix=prop_suffix, user_active_code='1')
        db.session.add(userLease)
        db.session.commit()

    if (access_code!=''):
        FAccessData = Accessdata.query.filter_by(access_code=access_code, assign_flag='0', active_flag='1').first()
        if FAccessData:
            FAccessData.assign_user_id = user.id
            FAccessData.assign_flag = '1'
            db.session.commit()

        #accessCodeData = Usercode.query.filter_by(user_id=user.id, user_active_code='1').first()
        #if accessCodeData:
        #    accessCodeData.user_active_code = '0'
        #    db.session.commit()

        userLease = Usercode(user_id=user.id, user_code=access_code, user_prop_id='', user_prop_name='', user_prop_suffix='', user_active_code='0')
        db.session.add(userLease)
        db.session.commit()


    return jsonify({
        'returnFlag':'Success', 
        'message': "User created",
        'user': {
            'usertype_id': usertype_id, 'username': username, "email": email, "firstname": firstname, "lastname": lastname, "phoneno": phoneno
        }

    }), HTTP_201_CREATED

@auth.post('/login')
@jwt_required()
@swag_from('./docs/auth/login.yaml')
def login():
    if (request.json.get('DirectLogin', '')) :
        LoginID = request.json.get('LoginID', '')

        user = db.session.query(User.id, User.firstname, User.lastname, User.email , User.usertype_id , User.password, User.phoneno, Usertype.usertype).filter_by(id=LoginID).join(Usertype).first()
    else:
        email = request.json.get('email', '')
        password = request.json.get('password', '')

        user = db.session.query(User.id, User.firstname, User.lastname, User.email , User.usertype_id , User.password, User.phoneno, Usertype.usertype).filter_by(email=email).join(Usertype).first()

    if user:
        if (request.json.get('DirectLogin', '')) :
            is_pass_correct = True
        else:
            is_pass_correct = check_password_hash(user.password, password)

        if is_pass_correct:
            #refresh = create_refresh_token(identity=user.id)
            #access = create_access_token(identity=user.id)

            additional_claims = {"user_id": user.id, "email": user.email, "firstname": user.firstname, "lastname": user.lastname}
            access_token = create_access_token('General', additional_claims=additional_claims)

            user_code = ''
            user_management_id = user.id
            user_prop_id = ''
            user_prop_name = ''
            user_prop_suffix = ''

            userlease = Usercode.query.filter_by(user_id=user.id, user_active_code = '1').first()
            if userlease:
                user_code = userlease.user_code
                user_prop_id = userlease.user_prop_id
                user_prop_name = userlease.user_prop_name
                user_prop_suffix = userlease.user_prop_suffix

            FeatureAccess = []
            if (user_code!=''):
                FeatureAccessData = Accessdata.query.filter_by(assign_user_id=user.id).first()
                if FeatureAccessData:
                    FeatureAccess = json.loads(FeatureAccessData.feature_access)
                    user_management_id = FeatureAccessData.user_id
            

            return jsonify({
                'error': '',
                'user': {
                    #'refresh': refresh,
                    'access_token': access_token,
                    'id': user.id,
                    'firstname': user.firstname,
                    'lastname': user.lastname,
                    'phoneno': user.phoneno,
                    'usertype_id': user.usertype_id,
                    'usertype_name': user.usertype,
                    'user_code': user_code,
                    'user_management_id': user_management_id,
                    'user_prop_id': user_prop_id,
                    'user_prop_name': user_prop_name,
                    'user_prop_suffix': user_prop_suffix,
                    'email': user.email,
                    'FeatureAccess': FeatureAccess,
                }

            }), HTTP_200_OK
        else:
            return jsonify({'error': 'Wrong credentials'}), HTTP_200_OK
    else:
        return jsonify({'error': 'User Not Exists'}), HTTP_200_OK

@auth.get("/getByID/<int:id>")
@jwt_required()
def getUserByIDme(id):
    user = User.query.filter_by(id=id).first()

    if not user:
        return jsonify({'message': 'Item not found'}), HTTP_200_OK

    user_code = ''
    propManagementID = ''
    propManagementName = ''
    #Tenant
    if (user.usertype_id==1) :
        userlease = Usercode.query.filter_by(user_id=user.id, user_active_code='1').first()
        if (userlease):
            user_code = userlease.user_code
            #getPropertyData = Propertydata.query.filter_by(id=userlease.user_prop_id).first()
            getPropertyData = db.session.query(Propertydata.user_id, User.firstname, User.lastname).join(User, User.id == Propertydata.user_id).filter(Propertydata.id==userlease.user_prop_id).first()
            propManagementID = getPropertyData.user_id
            propManagementName = getPropertyData.firstname + ' ' + getPropertyData.lastname

    #Staff
    if (user.usertype_id==3) :
        userlease = Usercode.query.filter_by(user_id=user.id).first()
        if (userlease):
            user_code = userlease.user_code


    paydata = []
    items = db.session.query(Userpaymethod).filter(Userpaymethod.user_id == id).all()

    for item in items:
        f = Fernet(item.fernet_key)
        cardDataTok = f.decrypt(item.stripe_token_data).decode()
        cardData = json.loads(cardDataTok)

        c_cvc = "***" #int(cardData['cvc'])
        c_no = "**** **** **** " + cardData['number'][-4:]
        c_date = str(cardData['exp_month']) + "/" + str(cardData['exp_year'])


        #if (item.stripe_pay_method_id!='') :
        #    payStripe = stripe.PaymentMethod.retrieve(
        #        item.stripe_pay_method_id,
        #    )

        #    c_no = "************" + payStripe.card.last4
        #    c_date = str(payStripe.card.exp_month) + "/" + str(payStripe.card.exp_year)

        paydata.append({
            "id" : item.id, 
            "default_method" : item.default_method, 
            "stripe_pay_method_id" : item.stripe_pay_method_id, 
            "pay_name" : item.pay_name,
            "c_no" : c_no,
            "c_date" : c_date,
            "c_cvc" : c_cvc,
            })

    return jsonify({
        'id': user.id,
        'usertype_id': user.usertype_id,
        'username': user.username,
        'email': user.email,
        'password': user.password,
        'firstname': user.firstname,
        'lastname': user.lastname,
        'phoneno': user.phoneno,
        'user_code': user_code,
        'propManagementID': propManagementID,
        'propManagementName': propManagementName,
        'stripe_api_key': user.stripe_api_key,
        'comm_percentage': user.comm_percentage,
        'paydata': paydata,
    }), HTTP_200_OK

@auth.get("/getPayInfoByID/<string:type>/<int:id>")
@jwt_required()
def getUserPayInfoByID(type, id):
    if (type=='all'):
        paydata = []
        userpay = db.session.query(Userpaymethod).filter(Userpaymethod.user_id == id).all()
        if not userpay:
            return jsonify({
                'itemFlag': False,
                'message': 'Item not found',
            }), HTTP_200_OK
        else:
            SelCardID = ''
            for item in userpay:
                f = Fernet(item.fernet_key)
                cardDataTok = f.decrypt(item.stripe_token_data).decode()
                cardData = json.loads(cardDataTok)

                c_cvc = "***" #int(cardData['cvc'])
                c_no = "**** **** **** " + cardData['number'][-4:]
                c_date = str(cardData['exp_month']) + "/" + str(cardData['exp_year'])


                #payStripe = stripe.PaymentMethod.retrieve(
                #    item.stripe_pay_method_id,
                #)

                #c_cvc = "***"
                #c_no = "**** **** ****" + payStripe.card.last4
                #c_date = str(payStripe.card.exp_month) + "/" + str(payStripe.card.exp_year)

                if (item.default_method==1):
                    SelCardID = item.id

                paydata.append({
                    "id" : item.id, 
                    "default_method" : item.default_method, 
                    "stripe_pay_method_id" : item.stripe_pay_method_id, 
                    "pay_name" : item.pay_name,
                    "c_no" : c_no,
                    "c_date" : c_date,
                    "c_cvc" : c_cvc,
                    })

            return jsonify({
                    'itemFlag': True,
                    'SelCardID': SelCardID,
                    'paydata': paydata
                }), HTTP_200_OK

    else:
        userpay = Userpaymethod.query.filter_by(user_id=id, default_method='1').first()

        if not userpay:
            return jsonify({
                'itemFlag': False,
                'message': 'Item not found',
            }), HTTP_200_OK
        else:
            f = Fernet(userpay.fernet_key)
            cardDataTok = f.decrypt(userpay.stripe_token_data).decode()
            cardData = json.loads(cardDataTok)

            c_cvc = "***" #int(cardData['cvc'])
            c_no = "**** **** **** " + cardData['number'][-4:]
            c_date = str(cardData['exp_month']) + "/" + str(cardData['exp_year'])

            #payStripe = stripe.PaymentMethod.retrieve(
            #    userpay.stripe_pay_method_id,
            #)

            #c_no = "**** **** ****" + payStripe.card.last4
            #c_date = str(payStripe.card.exp_month) + "/" + str(payStripe.card.exp_year)

            return jsonify({
                'itemFlag': True,
                'id': userpay.id,
                'stripe_pay_method_id': userpay.stripe_pay_method_id,
                'pay_name': userpay.pay_name,
                'c_no': c_no,
                'c_date': c_date,
            }), HTTP_200_OK

@auth.get("/allusertype")
@jwt_required()
def get_allusertype():
    data = []
    items = Usertype.query.filter_by().all()
    for item in items:
        data.append({"utype_id" : item.id, "utype_name" : item.usertype, "utype_desc" : item.usertype_desc })

    return jsonify(data), HTTP_200_OK

@auth.get('/token/refresh')
#@jwt_required(refresh=True)
@swag_from('./docs/auth/token_refresh.yaml')
def refresh_users_token():
    #identity = get_jwt_identity()
    additional_claims = {"user_id": 0, "email": '', "firstname": '', "lastname": ''}
    access = create_access_token('General', additional_claims=additional_claims)#identity=identity

    return jsonify({
        'accessCode': access
    }), HTTP_200_OK

@auth.put("/updateLeaseCode")
@auth.patch("/updateLeaseCode")
@jwt_required()
def updateLeaseCode():
    LeaseCode = request.json.get('LeaseCode', '')
    user_id = request.json.get('user_id', '')

    property = db.session.query(Propertydata).join(Propertyleases, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==LeaseCode).first()

    if not property:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    leaseCode = Usercode.query.filter_by(user_id=user_id, user_active_code='1').first()
    if leaseCode:
        leaseCode.user_active_code = '0'
        db.session.commit()


    userInfo = db.session.query(User).filter(User.id==user_id).first()
    TenantAddNotification(property.user_id, LeaseCode, userInfo.firstname + ' ' + userInfo.lastname, property.id)


    leaseCodeData = Usercode.query.filter_by(user_id=user_id, user_code=LeaseCode).first()
    if leaseCodeData:
        leaseCodeData.user_active_code = '1'
        db.session.commit()
    else:
        userLease = Usercode(user_id=user_id, user_code=LeaseCode, user_prop_id=property.id, user_prop_name=property.name, user_prop_suffix=property.suffix, user_active_code='1')
        db.session.add(userLease)
        db.session.commit()

    return jsonify({
        'id': user_id,
        'user_code': LeaseCode,
        'user_prop_id': property.id,
        'user_prop_name': property.name,
        'user_prop_suffix': property.suffix,
    }), HTTP_200_OK

@auth.put("/updatePaymentDetail")
@auth.patch("/updatePaymentDetail")
@jwt_required()
def updatePaymentDetail():
    current_user = request.json.get('user_id', '')

    getData = User.query.filter_by(id=current_user).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    #PMCVC = request.json.get('pm_cvc', '')
    CNo = request.json.get('c_no', '')
    pay_exp_month = request.json.get('pay_exp_month', '')
    pay_exp_year = request.json.get('pay_exp_year', '')

    default_method = request.json.get('default_method', '')

    pay_name = request.json.get('pay_name', '')

    cardData = {"number": CNo, "exp_month": pay_exp_month, "exp_year": pay_exp_year}
    cardDataTok = json.dumps(cardData)

    key = Fernet.generate_key()
    f = Fernet(key)
    encoded_message = cardDataTok.encode()
    token = f.encrypt(encoded_message)

    if (default_method=='1'):
        defData = db.session.query(Userpaymethod).filter(Userpaymethod.user_id==current_user).all()
        for item in defData:
            noData = Userpaymethod.query.filter_by(id=item.id).first()
            noData.default_method = '0'
            db.session.commit()

    userpay = Userpaymethod(user_id=current_user, stripe_pay_method_id='', fernet_key=key, stripe_token_data=token, pay_name=pay_name, default_method=default_method)
    db.session.add(userpay)
    db.session.commit()

    return jsonify({
        'id': getData.id,
    }), HTTP_200_OK

@auth.put("/detachMethod")
@auth.patch("/detachMethod")
@jwt_required()
def detachMethod():
    user_id = request.json.get('user_id', '')
    MID = request.json.get('MID', '')

    getData = Userpaymethod.query.filter_by(id=MID, user_id=user_id).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    #stripe.PaymentMethod.detach(
    #    getData.stripe_pay_method_id,
    #)

    db.session.query(Userpaymethod).filter_by(id=MID, user_id=user_id).delete(synchronize_session=False)
    db.session.commit()

    return jsonify({
        'id': MID,
    }), HTTP_200_OK

@auth.put("/MakeDefault")
@auth.patch("/MakeDefault")
@jwt_required()
def MakeDefault():
    user_id = request.json.get('user_id', '')
    MID = request.json.get('MID', '')

    getData = Userpaymethod.query.filter_by(user_id=user_id).all()
    for item in getData:
        noData = Userpaymethod.query.filter_by(id=item.id).first()
        noData.default_method = '0'
        db.session.commit()


    getData = Userpaymethod.query.filter_by(id=MID, user_id=user_id).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    getData.default_method = '1'
    db.session.commit()
    
    return jsonify({
        'id': MID,
    }), HTTP_200_OK

@auth.put("/updateResetCode")
@auth.patch("/updateResetCode")
@jwt_required()
def updateResetCode():
    resetcode = request.json.get('reset_code', '')
    useremail = request.json.get('email', '')

    getData = User.query.filter_by(email=useremail).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    getData.reset_code = resetcode

    db.session.commit()

    return jsonify({
        'id': getData.id,
        'user_reset_code': getData.reset_code,
    }), HTTP_200_OK

@auth.put("/updateNewPwd")
@auth.patch("/updateNewPwd")
@jwt_required()
def updateNewPwd():
    resetcode = request.json.get('user_ResetCode', '')
    userpassword = request.json.get('password1', '')

    getData = User.query.filter_by(reset_code=resetcode).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    getData.password = generate_password_hash(userpassword)
    getData.reset_code = ''

    db.session.commit()

    return jsonify({
        'id': getData.id,
        'user_reset_code': getData.reset_code,
    }), HTTP_200_OK

@auth.put("/updateUser")
@auth.patch("/updateUser")
@jwt_required()
def updateUser():

    user_type_id = request.json.get('user_type_id', '')

    firstname = request.json.get('firstname', '')
    lastname = request.json.get('lastname', '')
    phoneno = request.json.get('phoneno', '')
    user_id = request.json.get('user_id', '')

    FeatureAccess = []
    lease_code = ''
    user_management_id = user_id
    prop_id = ''
    prop_name = ''
    prop_suffix = ''
    prop_UserID = ''

    if (request.json.get('user_code', '')) :
        lease_code = request.json.get('user_code', '')

    getData = User.query.filter_by(id=user_id).first()
    if not getData:
        return jsonify({'returnFlag':'error','message': 'User not found'}), HTTP_200_OK

    if (lease_code!=''):
        #Tenant
        if (user_type_id=='1'):
            property = db.session.query(Propertydata).join(Propertyleases, Propertyleases.property_id == Propertydata.id).filter(Propertyleases.lease_code==lease_code).first()
            if property:
                prop_id = property.id
                prop_name = property.name
                prop_suffix = property.suffix
                prop_UserID = property.user_id
            else:
                return jsonify({'returnFlag':'error', 'message': 'Lease Code not valid'}), HTTP_200_OK

        #Staff
        if (user_type_id=='3'):
            aCodeData = Usercode.query.filter_by(user_id=user_id).first()
            if aCodeData:
                if (aCodeData.user_code==lease_code):
                    FAccessData = Accessdata.query.filter_by(access_code=lease_code).first()
                    if FAccessData:
                        FeatureAccess = json.loads(FAccessData.feature_access)
                        user_management_id = FAccessData.user_id

                        FAccessData.assign_user_id = user_id
                        FAccessData.assign_flag = '1'
                        db.session.commit()

                    else:
                        return jsonify({'returnFlag':'error', 'message': 'Access Code not valid'}), HTTP_200_OK
                else:
                    FAccessData = Accessdata.query.filter_by(access_code=lease_code, assign_flag='0', active_flag='1').first()
                    if FAccessData:
                        FeatureAccess = json.loads(FAccessData.feature_access)
                        user_management_id = FAccessData.user_id

                        FAccessData.assign_user_id = user_id
                        FAccessData.assign_flag = '1'
                        db.session.commit()

                        OldAccessData = Accessdata.query.filter_by(access_code=aCodeData.user_code).first()
                        if OldAccessData:
                            OldAccessData.assign_user_id = '0'
                            OldAccessData.assign_flag = '0'
                            db.session.commit()
                    else:
                        return jsonify({'returnFlag':'error', 'message': 'Access Code not valid'}), HTTP_200_OK
            else:
                FAccessData = Accessdata.query.filter_by(access_code=lease_code, assign_flag='0', active_flag='1').first()
                if FAccessData:
                    FeatureAccess = json.loads(FAccessData.feature_access)
                    user_management_id = FAccessData.user_id

                    FAccessData.assign_user_id = user_id
                    FAccessData.assign_flag = '1'
                    db.session.commit()

                else:
                    return jsonify({'returnFlag':'error', 'message': 'Access Code not valid'}), HTTP_200_OK


    stripe_api_key = getData.stripe_api_key
    
    if (request.json.get('stripe_api_key', '')) :
        stripe_api_key = request.json.get('stripe_api_key', '')

    getData.firstname = firstname
    getData.lastname = lastname
    getData.phoneno = phoneno
    getData.stripe_api_key = stripe_api_key

    db.session.commit()


    if (lease_code!=''):
        DisableCodeFlag = '0'
        #Staff
        if (user_type_id=='3'):
            leaseCodeData = Usercode.query.filter_by(user_id=user_id).first()
            if leaseCodeData:
                if (leaseCodeData.user_code!=lease_code):
                    leaseCodeData.user_active_code = '0'
                    DisableCodeFlag = '1'
                else:
                    if (leaseCodeData.user_active_code==0):
                        DisableCodeFlag = '1'

                leaseCodeData.user_code = lease_code
                db.session.commit()
            else:
                userLease = Usercode(user_id=user_id, user_code=lease_code, user_prop_id=prop_id, user_prop_name=prop_name, user_prop_suffix=prop_suffix, user_active_code='0')
                db.session.add(userLease)
                db.session.commit()
                DisableCodeFlag = '1'
            
            if (DisableCodeFlag=='1'):
                FeatureAccess = []

        #Tenant
        if (user_type_id=='1'):
            leaseCodeData = Usercode.query.filter_by(user_id=user_id, user_active_code='1', user_code=lease_code).first()
            if not leaseCodeData:
                TenantAddNotification(prop_UserID, lease_code, firstname + ' ' + lastname, prop_id)

                leaseCode = Usercode.query.filter_by(user_id=user_id, user_active_code='1').first()
                if leaseCode:
                    leaseCode.user_active_code = '0'
                    db.session.commit()

            leaseCodeData = Usercode.query.filter_by(user_id=user_id, user_code=lease_code).first()
            if leaseCodeData:
                leaseCodeData.user_active_code = '1'
                db.session.commit()
            else:
                userLease = Usercode(user_id=user_id, user_code=lease_code, user_prop_id=prop_id, user_prop_name=prop_name, user_prop_suffix=prop_suffix, user_active_code='1')
                db.session.add(userLease)
                db.session.commit()

    return jsonify({
        'returnFlag':'success',
        'id': getData.id,
        'firstname': getData.firstname,
        'lastname': getData.lastname,
        'phoneno': getData.phoneno,
        'user_code': lease_code,
        'user_management_id': user_management_id,
        'user_prop_id': prop_id,
        'user_prop_name': prop_name,
        'user_prop_suffix': prop_suffix,
        'DisableCodeFlag': DisableCodeFlag,
        'FeatureAccess': FeatureAccess,
    }), HTTP_200_OK

@auth.put("/changePwd")
@auth.patch("/changePwd")
@jwt_required()
def changePwd():
    user_id = request.json.get('user_id', '')
    userpassword = request.json.get('password1', '')

    getData = User.query.filter_by(id=user_id).first()

    if not getData:
        return jsonify({'message': 'User not found'}), HTTP_404_NOT_FOUND

    getData.password = generate_password_hash(userpassword)

    db.session.commit()

    return jsonify({
        'id': getData.id,
        'firstname': getData.firstname,
    }), HTTP_200_OK

@auth.put("/updateSelProp")
@auth.patch("/updateSelProp")
@jwt_required()
def updateSelProp():
    propID = request.json.get('propID', '')
    user_id = request.json.get('user_id', '')

    getPropertyData = Propertydata.query.filter_by(id=propID).first()

    if not getPropertyData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    #propID = getPropertyData.id
    propName = getPropertyData.name
    propSuffix = getPropertyData.suffix


    getData = Usercode.query.filter_by(user_id=user_id).first()

    if not getData:
        userlease = Usercode(user_id=user_id, user_code='', user_prop_id=propID, user_prop_name=propName, user_prop_suffix=propSuffix, user_active_code='1')
        db.session.add(userlease)
        db.session.commit()
    else :
        getData.user_prop_id = propID
        getData.user_prop_name = propName
        getData.user_prop_suffix = propSuffix
        getData.user_active_code = '1'
        
        db.session.commit()


    return jsonify({
        'id': user_id,
        'user_prop_id': propID,
        'user_prop_name': propName,
        'user_prop_suffix': propSuffix,
    }), HTTP_200_OK

@auth.get("/UserLeaseInfo/<int:userid>/<int:usertypeid>")
@jwt_required()
def getUserLeaseInfo(userid, usertypeid):

    retData = []
    LeaseData = db.session.query(Usercode).filter(Usercode.user_id == userid, Usercode.user_active_code == '0').order_by(Usercode.id.desc()).all()
    for item in LeaseData:
        if (usertypeid==1):
            new_link = {
                'user_code': item.user_code,
                'user_prop_name': item.user_prop_name,
                'user_prop_suffix': item.user_prop_suffix,
                'disp_name': '[' + item.user_prop_suffix + ' - ' + item.user_prop_name + ']',
            }
            retData.append(new_link)

        #elif (usertypeid==3):
        #    new_link = {
        #        'user_code': item.user_code,
        #        'user_prop_name': item.user_prop_name,
        #        'user_prop_suffix': item.user_prop_suffix,
        #        'disp_name': '',
        #    }
        #    retData.append(new_link)


    return jsonify({
        'retData': retData,
    }), HTTP_200_OK

@auth.put("/UserSearchResult")
@jwt_required()
def getUserSearchResult():
    searchQuery = request.json.get('searchQuery', '')
    UserTypeID = int(request.json.get('UserTypeID', ''))
    UserID = int(request.json.get('UserID', ''))

    retData = []

    #management
    #if ((UserTypeID==2) or (UserTypeID==3)):
    #    SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype).join(Usertype, Usertype.id == User.usertype_id).filter(User.usertype_id.in_([3,1,2])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'))).group_by(User.id).all()
    #else:
    #    SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype).join(Usertype, Usertype.id == User.usertype_id).filter(User.usertype_id.in_([3,1,2])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'))).group_by(User.id).all()

    SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype).join(Usertype, Usertype.id == User.usertype_id).filter(User.id != UserID).filter(User.usertype_id.in_([3,1,2])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'))).group_by(User.id).all()
    for item in SearchData:
        new_link = {
            'id': item.id,
            'name': item.firstname + " " + item.lastname,
            'role': item.usertype,
            'user_code': '',
            'property_id': '',
            'floorsno': '',
            'aptno': '',
            'val': '',
            'isChecked': False,
        }

        retData.append(new_link)


    #Management and staff
    if ((UserTypeID==2) or (UserTypeID==3)):
        SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype, Usercode.user_code, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno).join(Usertype, Usertype.id == User.usertype_id).join(Usercode, Usercode.user_id == User.id).join(Propertyleases, Usercode.user_code == Propertyleases.lease_code).join(Propertydata, Propertydata.id == Propertyleases.property_id).filter(User.usertype_id.in_([1])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'), Propertyleases.aptno.like('%'+searchQuery+'%'))).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.lease_code, User.id).all()
    else:
        SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype, Usercode.user_code, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno).join(Usertype, Usertype.id == User.usertype_id).join(Propertydata, Propertydata.user_id == User.id).join(Propertyleases, Propertyleases.property_id == Propertydata.id).join(Usercode, Usercode.user_code == Propertyleases.lease_code).filter(Usercode.user_id==UserID).filter(User.usertype_id.in_([2])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'))).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.lease_code, User.id).all()
        #SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype, Usercode.user_code, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno).join(Usertype, Usertype.id == User.usertype_id).join(Usercode, Usercode.user_id == User.id, isouter=True).join(Propertyleases, Usercode.user_code == Propertyleases.lease_code, isouter=True).filter(User.usertype_id.in_([1])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'), Propertyleases.aptno.like('%'+searchQuery+'%'))).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.lease_code, User.id).all()

        #SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype).join(Usertype, Usertype.id == User.usertype_id).filter(User.usertype_id.in_([2])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'))).group_by(User.id).all()
        #SearchData = db.session.query(User.id, User.firstname, User.lastname, Usertype.usertype, Usercode.user_code, Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno).join(Usertype, Usertype.id == User.usertype_id).join(Usercode, Usercode.user_id == User.id, isouter=True).join(Propertyleases, Usercode.user_code == Propertyleases.lease_code, isouter=True).filter(User.usertype_id.in_([1])).filter(or_(User.firstname.like('%'+searchQuery+'%'), User.lastname.like('%'+searchQuery+'%'), Propertyleases.aptno.like('%'+searchQuery+'%'))).group_by(Propertyleases.property_id, Propertyleases.floorsno, Propertyleases.aptno, Propertyleases.lease_code, User.id).all()

    for item in SearchData:
        new_link = {
            'id': item.id,
            'name': item.firstname + " " + item.lastname,
            'role': item.usertype,
            'user_code': item.user_code,
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'val': '',
            'isChecked': False,
        }

        retData.append(new_link)

    return jsonify({
        'retData': retData,
    }), HTTP_200_OK

def TenantAddNotification(MUserID, LCode, UserName, PropID):

    noto_add = Notification(
                    user_id=MUserID, 
                    n_type='Normal', 
                    n_date=date.today(), 
                    n_property_id=PropID, 
                    n_lease_code=LCode, 
                    n_title='New', 
                    n_subtitle='Tenant', 
                    n_desc='A new tenant, <b>'+ UserName +'</b> has joined your lease <b>"'+ LCode +'"</b>. Tap here to see the messaging section.', 
                    n_link="/messaging", 
                    n_read=0, 
                    n_clear=0)
    db.session.add(noto_add)
    db.session.commit()

    leaseCodeData = Usercode.query.filter_by(user_active_code='1', user_code=LCode).all()
    for item in leaseCodeData:
        noto_add = Notification(
                        user_id=item.user_id, 
                        n_type='Normal', 
                        n_date=date.today(), 
                        n_property_id=PropID, 
                        n_lease_code=LCode, 
                        n_title='New', 
                        n_subtitle='Tenant', 
                        n_desc='A new tenant, <b>'+ UserName +'</b> has joined your lease <b>"'+ LCode +'"</b>. Tap here to see the lease details.', 
                        n_link="/manage-leases", 
                        n_read=0, 
                        n_clear=0)
        db.session.add(noto_add)
        db.session.commit()

@auth.put("/setcommission")
@auth.patch("/setcommission")
@jwt_required()
def setcommission():
    id = request.json.get('id', '')
    comm_percentage = request.json.get('comm_percentage', '')

    getData = User.query.filter_by(id=id).first()

    getData.comm_percentage = comm_percentage
    db.session.commit()


    return jsonify({
        'id': getData.id,
    }), HTTP_200_OK

@auth.get("/allstaffbyuser/<int:userid>")
@jwt_required()
def get_allStaff(userid):
    StaffsData = []

    #if (userid==0):
    SData = db.session.query(User).filter(User.usertype_id == 3).order_by(User.id.desc()).all()
    #else:
    #    SData = db.session.query(User).filter(User.usertype_id == 3).filter(User.user_id == userid).order_by(User.id.desc()).all()

    for item in SData:
        accessCodeData = []
        #FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        #for subitem in FilesData:
        #    new_add = {
        #        'file_name': subitem.file_name,
        #    }
        #    accessCodeData.append(new_add)

        new_link = {
            'id': item.id,
            'firstname': item.firstname,
            'lastname': item.lastname,
            'email': item.email,
            'phoneno': item.phoneno,
            'accessCode': accessCodeData,
        }

        StaffsData.append(new_link)

    return jsonify({
        'StaffsData': StaffsData,
    }), HTTP_200_OK

@auth.route('/accesscode/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_accesscode():

    if request.method == 'POST':
        user_id = request.get_json().get('user_id', '')
        access_code = request.get_json().get('access_code', '')
        assign_flag = request.get_json().get('assign_flag', '')
        assign_user_id = request.get_json().get('assign_user_id', '')
        active_flag = request.get_json().get('active_flag', '')
        feature_access = request.get_json().get('feature_access', '')

        AccessData = Accessdata.query.filter_by(access_code=access_code).first()
        if AccessData:
            return jsonify({
                'InsertFlag': False,
                'msg': "Access Code allready exists.....",
            }), HTTP_201_CREATED
        else:
            dataAdd = Accessdata(user_id=user_id, access_code=access_code, assign_flag=assign_flag, assign_user_id=assign_user_id, active_flag=active_flag, feature_access= json.dumps(feature_access) )
            db.session.add(dataAdd)
            db.session.commit()

            return jsonify({
                'InsertFlag': True,
                'id': dataAdd.id,
            }), HTTP_201_CREATED

    else:
        UserID = request.args.get('UserID', 1, type=int)

        items = db.session.query(Accessdata.id, Accessdata.access_code, Accessdata.feature_access, User.firstname, User.lastname, User.email, User.phoneno).join(User, User.id == Accessdata.assign_user_id, isouter=True).filter(Accessdata.user_id == UserID).group_by(Accessdata.id).all()

        data = []
        for item in items:
            assign_username = ''
            if (item.firstname):
                assign_username = item.firstname + " " + item.lastname

            assign_useremail = ''
            if (item.email):
                assign_useremail = item.email

            assign_userphoneno = ''
            if (item.phoneno):
                assign_userphoneno = item.phoneno

            assign_user_active_code = '0'
            ACode = Usercode.query.filter_by(user_code=item.access_code).first()
            if ACode:
                assign_user_active_code = ACode.user_active_code


            FeatureAccess = []
            FeatureAccess.append(json.loads(item.feature_access))
            data.append({
                'id': item.id,
                'access_code': item.access_code,
                'FeatureAccess': FeatureAccess,
                'assign_username': assign_username,
                'assign_useremail': assign_useremail,
                'assign_userphoneno': assign_userphoneno,
                'assign_user_active_code': assign_user_active_code,
            })

        return jsonify(data), HTTP_200_OK

@auth.put("/toggleactivecode")
@auth.patch("/toggleactivecode")
@jwt_required()
def toggleactivecode():
    access_code = request.json.get('access_code', '')

    getData = Usercode.query.filter_by(user_code=access_code).first()
    
    if (getData.user_active_code==0):
        getData.user_active_code = '1'
    elif (getData.user_active_code==1):
        getData.user_active_code = '0'

    db.session.commit()

    return jsonify({
        'doneFlag': 'Success',
    }), HTTP_200_OK
