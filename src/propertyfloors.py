from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import Propertyfloordata, db, Propertyleases
from datetime import date, datetime

propertysfloors = Blueprint("propertysfloors", __name__, url_prefix="/api/v1/propertyfloors")

@propertysfloors.route('/', methods=['POST'])
@jwt_required()
def handle_propertysfloors():
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    #if current_user == 0:
    #    return jsonify({
    #        'error': "Login not exists"
    #    }), HTTP_200_OK

    if current_user == 0:
        current_user = request.get_json().get('current_user', '')

    property_id = request.get_json().get('property_id', '')
    floorsno = request.get_json().get('floorsno', '')
    aptno = request.get_json().get('aptno', '')

    propertyfloors = Propertyfloordata(property_id=property_id, 
                        floorsno=floorsno, 
                        aptno=aptno, 
                        user_id=current_user)
    db.session.add(propertyfloors)
    db.session.commit()

    return jsonify({
        'id': propertyfloors.id,
        'user_id': propertyfloors.user_id,
        'property_id': propertyfloors.property_id,
        'floorsno': propertyfloors.floorsno,
        'aptno': propertyfloors.aptno,
        'created_at': propertyfloors.created_at,
        'updated_at': propertyfloors.updated_at,
    }), HTTP_201_CREATED
        
@propertysfloors.delete("/deletebyprop/<int:propid>/<int:user_id>")
@jwt_required()
def delete_propertysfloorsbypropid(propid, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    db.session.query(Propertyfloordata).filter_by(property_id=propid).delete(synchronize_session=False)
    db.session.commit()

    return jsonify({}), HTTP_204_NO_CONTENT

@propertysfloors.delete("/<int:id>")
@jwt_required()
def delete_propertysfloors(id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        return jsonify({
            'error': "Login not exists"
        }), HTTP_200_OK

    propertyfloors = Propertyfloordata.query.filter_by(id=id).first()#user_id=current_user, 

    if not propertyfloors:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    db.session.delete(propertyfloors)
    db.session.commit()

    return jsonify({}), HTTP_204_NO_CONTENT

@propertysfloors.put('/<int:id>')
@propertysfloors.patch('/<int:id>')
@jwt_required()
def editpropertyfloors(id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        return jsonify({
            'error': "Login not exists"
        }), HTTP_200_OK

    propertyfloors = Propertyfloordata.query.filter_by(id=id).first()#user_id=current_user, 

    if not propertyfloors:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    property_id = request.get_json().get('property_id', '')
    floorsno = request.get_json().get('floorsno', '')
    aptno = request.get_json().get('aptno', '')

    propertyfloors.property_id = property_id
    propertyfloors.floorsno = floorsno
    propertyfloors.aptno = aptno

    db.session.commit()

    return jsonify({
        'id': propertyfloors.id,
        'property_id': propertyfloors.property_id,
        'floorsno': propertyfloors.floorsno,
        'aptno': propertyfloors.aptno,
        'created_at': propertyfloors.created_at,
        'updated_at': propertyfloors.updated_at,
    }), HTTP_200_OK

@propertysfloors.get("/allunitdata/<int:pid>/<int:userid>")
@jwt_required()
def get_alldata(pid, userid):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    #if current_user == 0:
    #    return jsonify({
    #        'error': "Login not exists"
    #    }), HTTP_200_OK

    if current_user == 0:
        current_user = userid

    data = []
    AllData = []
    items = Propertyfloordata.query.filter_by(property_id=pid).all()#user_id=current_user, 

    FloorsData = []
    #Unoccupied Apartments
    UnoccupiedAptData = []

    #Renewal Proposal
    RenewalAptData = []
    for item in items:
        if not  item.floorsno in FloorsData:
            FloorsData.append(item.floorsno)

    
    for FloorsNo in FloorsData:
        arrApt = []

        for item in items:
            if (item.floorsno == FloorsNo):
                arrApt.append(item.aptno)
                UnoccupiedAptData.append(item.aptno)

        newLinkAll = [FloorsNo,arrApt]
        AllData.append(newLinkAll)


    for item in items:
        new_link = {
            'property_id': item.property_id,
            'floorsno': item.floorsno,
            'aptno': item.aptno,
            'id': item.id,
        }

        data.append(new_link)

    return jsonify({
        'FloorsData': FloorsData,
        'UnoccupiedAptData': UnoccupiedAptData,
        'RenewalAptData': RenewalAptData,
        'Unitdata': data,
        'AllData': AllData,
    }), HTTP_200_OK

@propertysfloors.get("/aptdataRenewalUnoccupied/<int:pid>/<int:userid>")
@jwt_required()
def get_aptdataRenewalUnoccupied(pid, userid):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = userid

    currDt = date.today()
    #user_id=current_user, 
    items = Propertyfloordata.query.filter_by(property_id=pid).order_by(Propertyfloordata.floorsno, Propertyfloordata.aptno).all()

    FloorsData = []
    #Unoccupied Apartments
    UnoccupiedAptData = []
    #Renewal Proposal
    RenewalAptData = []

    for item in items:
        if not  item.floorsno in FloorsData:
            FloorsData.append(item.floorsno)
    
    for FloorsNo in FloorsData:
        for item in items:
            if (item.floorsno == FloorsNo):
                cntData = Propertyleases.query.filter(Propertyleases.lease_start_date <= currDt).filter(Propertyleases.lease_end_date >= currDt).filter(Propertyleases.lease_cancel==0).filter(Propertyleases.property_id==item.property_id).filter(Propertyleases.floorsno==item.floorsno).filter(Propertyleases.aptno==item.aptno).count()
                                                #

                new_link = {
                    'pid': item.property_id,
                    'floorsno': item.floorsno,
                    'aptno': item.aptno,
                    'id': item.id,
                    'cntData': cntData,
                }
                if (cntData==0):
                    UnoccupiedAptData.append(new_link)
                else:
                    RenewalAptData.append(new_link)

    return jsonify({
        'UnoccupiedAptData': UnoccupiedAptData,
        'RenewalAptData': RenewalAptData,
    }), HTTP_200_OK

@propertysfloors.get("/<int:id>/<int:user_id>")
@jwt_required()
def get_propertysfloor_data(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    itemData = Propertyfloordata.query.filter_by(id=id).first()#user_id=current_user, 

    if not itemData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'floorID': itemData.id,
        'floorPropID': itemData.property_id,
        'floorNo': itemData.floorsno,
        'floorAptNo': itemData.aptno,
    }), HTTP_200_OK

@propertysfloors.put("/checkExists")
@jwt_required()
def get_propertysfloor_databyaptNo():
    pid = request.json.get('PropID', '')
    aptno = request.json.get('AptNo', '')
    current_user = int(request.json.get('UserID', ''))

    itemData = Propertyfloordata.query.filter_by(property_id=pid, aptno=aptno).first()#user_id=current_user, 

    if not itemData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'floorID': itemData.id,
        'floorPropID': itemData.property_id,
        'floorNo': itemData.floorsno,
        'floorAptNo': itemData.aptno,
    }), HTTP_200_OK
