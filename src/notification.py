from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import Notification, db
from datetime import date, datetime
import datetime

notification = Blueprint("notification", __name__, url_prefix="/api/v1/notification")

@notification.get("/unreadcnt/<int:userid>")
@jwt_required()
def get_unreadcnt(userid):

    if (userid==''):
        return jsonify({
            'cntData': '0',
        }), HTTP_200_OK

    cntData = Notification.query.filter(Notification.user_id==userid).filter(Notification.n_clear=='0').count()

    SendData = []

    UrgentData = []
    getData = db.session.query(Notification).filter(Notification.n_type == 'Urgent').filter(Notification.n_clear=='0').filter(Notification.user_id == userid).all()
    for item in getData:

        new_link = {
            'id': item.id,
            'n_type': item.n_type,
            'n_date': item.n_date,
            'n_title': item.n_title,
            'n_subtitle': item.n_subtitle,
            'n_desc': item.n_desc,
            'n_link': item.n_link,
            'n_read': item.n_read,
            'n_clear': item.n_clear,
            'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
        }

        UrgentData.append(new_link)

    SendData.append({'Text' : "Urgent", 'CntData' : len(UrgentData), 'Data' : UrgentData});


    CurrentData = []
    currDt = date.today()
    getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.n_clear=='0').filter(Notification.user_id == userid).filter(Notification.n_date == currDt).all()
    for item in getData:

        new_link = {
            'id': item.id,
            'n_type': item.n_type,
            'n_date': item.n_date,
            'n_title': item.n_title,
            'n_subtitle': item.n_subtitle,
            'n_desc': item.n_desc,
            'n_link': item.n_link,
            'n_read': item.n_read,
            'n_clear': item.n_clear,
            'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
        }

        CurrentData.append(new_link)

    SendData.append({'Text' : "Today", 'CntData' : len(CurrentData), 'Data' : CurrentData});


    dtDataGet = db.session.query(Notification.n_date).filter(Notification.n_type == 'Normal').filter(Notification.n_clear=='0').filter(Notification.user_id == userid).filter(Notification.n_date != currDt).group_by(Notification.n_date).all()
    for itemDt in dtDataGet:
        CurrentData = []
        getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.n_clear=='0').filter(Notification.user_id == userid).filter(Notification.n_date == itemDt.n_date).all()
        for item in getData:

            new_link = {
                'id': item.id,
                'n_type': item.n_type,
                'n_date': item.n_date,
                'n_title': item.n_title,
                'n_subtitle': item.n_subtitle,
                'n_desc': item.n_desc,
                'n_link': item.n_link,
                'n_read': item.n_read,
                'n_clear': item.n_clear,
                'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
            }

            CurrentData.append(new_link)

        SendData.append({'Text' : (datetime.datetime.strptime(str(itemDt.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'), 'CntData' : len(CurrentData), 'Data' : CurrentData});


    return jsonify({
        'cntData': cntData,
        'SendData': SendData,
    }), HTTP_200_OK

@notification.get("/cleardata/<int:userid>")
@jwt_required()
def get_cleardata(userid):

    getData = db.session.query(Notification).filter(Notification.user_id==userid).filter(Notification.n_clear=='0').all()
    for item in getData:

        noData = Notification.query.filter_by(id=item.id).first()
        noData.n_clear = '1'
        db.session.commit()

    return jsonify({
        'FlagData': True,
    }), HTTP_200_OK

@notification.put("/alldatabyuser")
@jwt_required()
def get_alldatabyuser():
    UserID = request.json.get('UserID', '')
    UserTypeID = request.json.get('UserTypeID', '')
    LeaseCode = request.json.get('LeaseCode', '')

    SendData = []

    UrgentData = []
    if (UserTypeID=='1'):
        getData = db.session.query(Notification).filter(Notification.n_type == 'Urgent').filter(Notification.user_id == UserID).filter(Notification.n_lease_code == LeaseCode).all()
    else:
        getData = db.session.query(Notification).filter(Notification.n_type == 'Urgent').filter(Notification.user_id == UserID).all()

    for item in getData:

        new_link = {
            'id': item.id,
            'n_type': item.n_type,
            'n_date': item.n_date,
            'n_title': item.n_title,
            'n_subtitle': item.n_subtitle,
            'n_desc': item.n_desc,
            'n_link': item.n_link,
            'n_read': item.n_read,
            'n_clear': item.n_clear,
            'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
        }

        UrgentData.append(new_link)

    SendData.append({'Text' : "Urgent", 'CntData' : len(UrgentData), 'Data' : UrgentData});


    CurrentData = []
    currDt = date.today()
    if (UserTypeID=='1'):
        getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date == currDt).filter(Notification.n_lease_code == LeaseCode).all()
    else:
        getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date == currDt).all()

    for item in getData:

        new_link = {
            'id': item.id,
            'n_type': item.n_type,
            'n_date': item.n_date,
            'n_title': item.n_title,
            'n_subtitle': item.n_subtitle,
            'n_desc': item.n_desc,
            'n_link': item.n_link,
            'n_read': item.n_read,
            'n_clear': item.n_clear,
            'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
        }

        CurrentData.append(new_link)

    SendData.append({'Text' : "Today", 'CntData' : len(CurrentData), 'Data' : CurrentData});

    dtData = []
    if (UserTypeID=='1'):
        dtDataGet = db.session.query(Notification.n_date).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date != currDt).filter(Notification.n_lease_code == LeaseCode).group_by(Notification.n_date).all()
    else:
        dtDataGet = db.session.query(Notification.n_date).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date != currDt).group_by(Notification.n_date).all()

    for itemDt in dtDataGet:
        CurrentData = []
        if (UserTypeID=='1'):
            getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date == itemDt.n_date).filter(Notification.n_lease_code == LeaseCode).all()
        else:
            getData = db.session.query(Notification).filter(Notification.n_type == 'Normal').filter(Notification.user_id == UserID).filter(Notification.n_date == itemDt.n_date).all()

        for item in getData:

            new_link = {
                'id': item.id,
                'n_type': item.n_type,
                'n_date': item.n_date,
                'n_title': item.n_title,
                'n_subtitle': item.n_subtitle,
                'n_desc': item.n_desc,
                'n_link': item.n_link,
                'n_read': item.n_read,
                'n_clear': item.n_clear,
                'n_date_text': (datetime.datetime.strptime(str(item.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'),
            }

            CurrentData.append(new_link)

        SendData.append({'Text' : (datetime.datetime.strptime(str(itemDt.n_date), "%Y-%m-%d")).strftime('%b. %d %Y'), 'CntData' : len(CurrentData), 'Data' : CurrentData});


    return jsonify({
        'SendData': SendData,
        'dtData': dtData,
    }), HTTP_200_OK
