from flask.json import jsonify
from src.constants.http_status_codes import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from flask import Flask
from flask_cors import CORS, cross_origin
import os
from src.auth import auth
from src.propertys import propertys
from src.propertyfloors import propertysfloors
from src.propertyleases import propertyleases
from src.postdata import postdata
from src.faq import faqdata
from src.messaging import messaging
from src.notification import notification
from src.setting import setting
from src.database import db 
from flask_jwt_extended import JWTManager
from flasgger import Swagger
from src.config.swagger import template, swagger_config

def create_app(test_config=None):

    app = Flask(__name__, instance_relative_config=True)
    CORS(app, support_credentials=True)
    #app.config['CORS_HEADERS'] = 'Content-Type'
    #cors = CORS(app, resource={
    #    r"/*":{
    #        "origins":"*"
    #    }
    #})

    #@cross_origin(origin='*')
    #@cross_origin(supports_credentials=True)

    if test_config is None:
        app.config.from_mapping(
            SECRET_KEY=os.environ.get("SECRET_KEY"),
            SQLALCHEMY_DATABASE_URI=os.environ.get("SQLALCHEMY_DB_URI"),
            SQLALCHEMY_TRACK_MODIFICATIONS=False,
            JWT_SECRET_KEY=os.environ.get('JWT_SECRET_KEY'),

            SWAGGER={
                'title': "Housing App API",
                'uiversion': 3
            }
        )
    else:
        app.config.from_mapping(test_config)

    # Setup cors headers to allow all domains
    # https://flask-cors.readthedocs.io/en/latest/

    db.app = app
    db.init_app(app)

    JWTManager(app)
    
    app.register_blueprint(auth)
    app.register_blueprint(propertys)
    app.register_blueprint(propertysfloors)
    app.register_blueprint(propertyleases)
    app.register_blueprint(messaging)
    app.register_blueprint(postdata)
    app.register_blueprint(faqdata)
    app.register_blueprint(setting)
    app.register_blueprint(notification)

    Swagger(app, config=swagger_config, template=template)

    @app.errorhandler(HTTP_404_NOT_FOUND)
    def handle_404(e):
        return jsonify({'error': 'Not found'}), HTTP_404_NOT_FOUND

    @app.errorhandler(HTTP_500_INTERNAL_SERVER_ERROR)
    def handle_500(e):
        return jsonify({'error': 'Something went wrong, we are working on it'}), HTTP_500_INTERNAL_SERVER_ERROR

    return app
