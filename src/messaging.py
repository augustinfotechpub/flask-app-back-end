# from asyncio.windows_events import NULL
from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import Conversion, Conversionmsg, Conversionuser, Usertype, db, User, Propertydata, Usercode
from datetime import date, datetime
from sqlalchemy import func, null
from sqlalchemy import desc
from sqlalchemy.orm import load_only
from sqlalchemy import extract
from sqlalchemy import cast
import datetime
from sqlalchemy.engine import result
from sqlalchemy import or_

messaging = Blueprint("messaging", __name__, url_prefix="/api/v1/messaging")

@messaging.route('/addchat', methods=['POST'])
@jwt_required()
def addchat():
    current_user = request.get_json().get('current_user', '')
    InsertID = request.get_json().get('chatid', '')
    custom_message = request.get_json().get('custom_message', '')

    msgInsert = Conversionmsg(
                        msg_id=InsertID,
                        user_id=current_user, 
                        custom_message=custom_message)
    db.session.add(msgInsert)
    db.session.commit()

    return jsonify({
        'id': msgInsert.id,
    }), HTTP_201_CREATED
        
@messaging.route('/', methods=['POST', 'GET'])
@jwt_required()
def addsmsg():
    current_user = request.get_json().get('current_user', '')
    current_usertype = request.get_json().get('current_usertype', '')
    custom_message = request.get_json().get('custom_message', '')

    LoadRentAptDataMsg = request.get_json().get('LoadRentAptDataMsg', [])

    for item in LoadRentAptDataMsg:
        msg_type = item['msg_type']
        conv_type = 'Single'
        property_id = item['property_id']
        floorsno = item['floorsno']
        aptno = item['aptno']
        lease_code = item['lease_code']

        sendID = []
        sendID.append(int(current_user))
        sendID.append(int(item['senduser_id']))
        sendID.sort()

        ExistsData = db.session.query(Conversion.id).filter(Conversion.conv_type==conv_type).filter(Conversion.msg_type==msg_type).filter(Conversion.property_id==property_id).filter(Conversion.floorsno==floorsno).filter(Conversion.aptno==aptno).filter(Conversion.lease_code==lease_code).all()

        if not ExistsData:
            dataInsert = Conversion(
                                msg_type=msg_type,
                                conv_type=conv_type,
                                conv_active='1',
                                property_id=property_id, 
                                floorsno=floorsno, 
                                aptno=aptno, 
                                lease_code=lease_code)
            db.session.add(dataInsert)
            db.session.commit()

            InsertID = dataInsert.id

            msgInsert = Conversionmsg(
                                msg_id=InsertID,
                                user_id=current_user, 
                                custom_message=custom_message)
            db.session.add(msgInsert)
            db.session.commit()

            for sID in sendID:
                userInsert = Conversionuser(
                                    msg_id=InsertID,
                                    user_id=sID)
                db.session.add(userInsert)
                db.session.commit()

        else:
            #insertNew = True
            for MsgID in ExistsData:
                InsertID = MsgID['id']

                ExistsMsg = db.session.query(Conversionmsg.id).filter(Conversionmsg.msg_id==InsertID).filter(Conversionmsg.user_id==current_user).filter(Conversionmsg.custom_message==custom_message).all()
                if not ExistsMsg:
                    msgInsert = Conversionmsg(
                                        msg_id=InsertID,
                                        user_id=current_user, 
                                        custom_message=custom_message)
                    db.session.add(msgInsert)
                    db.session.commit()

                ExistsUser = db.session.query(Conversionuser.id).filter(Conversionuser.msg_id==InsertID).filter(Conversionuser.user_id==current_user).all()
                if not ExistsUser:
                    userInsert = Conversionuser(
                                        msg_id=InsertID,
                                        user_id=current_user)
                    db.session.add(userInsert)
                    db.session.commit()

                ExistsUser = db.session.query(Conversionuser.id).filter(Conversionuser.msg_id==InsertID).filter(Conversionuser.user_id==item['senduser_id']).all()
                if not ExistsUser:
                    userInsert = Conversionuser(
                                        msg_id=InsertID,
                                        user_id=item['senduser_id'])
                    db.session.add(userInsert)
                    db.session.commit()




                """ ExistsUserData = db.session.query(Conversionuser.user_id).filter(Conversionuser.msg_id==MsgID['id']).all()
                ExistsID = []
                for item in ExistsUserData:
                    ExistsID.append(item['user_id'])

                ExistsID.sort()

                if (sendID == ExistsID):
                    insertNew = False
                    InsertID = MsgID['id'] """

            #if (insertNew):
                """ dataInsert = Conversion(
                                    msg_type=msg_type,
                                    conv_type=conv_type,
                                    property_id=property_id, 
                                    floorsno=floorsno, 
                                    aptno=aptno, 
                                    lease_code=lease_code)
                db.session.add(dataInsert)
                db.session.commit()

                InsertID = dataInsert.id """

                """ msgInsert = Conversionmsg(
                                    msg_id=InsertID,
                                    user_id=current_user, 
                                    custom_message=custom_message)
                db.session.add(msgInsert)
                db.session.commit() """

                """ for sID in sendID:
                    userInsert = Conversionuser(
                                        msg_id=InsertID,
                                        user_id=sID)
                    db.session.add(userInsert)
                    db.session.commit() """

            """ else:
                msgInsert = Conversionmsg(
                                    msg_id=InsertID,
                                    user_id=current_user, 
                                    custom_message=custom_message)
                db.session.add(msgInsert)
                db.session.commit() """
    
    return jsonify({
        'current_user': current_user,
    }), HTTP_201_CREATED

@messaging.route('/startconversion', methods=['POST'])
@jwt_required()
def addstartconversion():
    current_user = request.get_json().get('current_user', '')
    UserTypeID = int(request.json.get('UserTypeID', ''))
    UserID = int(request.json.get('UserID', ''))

    SearchSelected = request.get_json().get('SearchSelected', [])

    Tcnt = 0
    Scnt = 0
    totCnt = 0
    sendID = []
    for item in SearchSelected:
        if (item['isChecked']):
            totCnt = totCnt + 1
            sendID.append(item['id'])
            if (item['role']=='Staff'):
                Scnt = Scnt + 1
            else:
                Tcnt = Tcnt + 1

    sendID.append(int(current_user))
    sendID.sort()

    if (totCnt==1):
        insertNew = True
        conv_type = 'Single'

        for item in SearchSelected:
            if (item['isChecked']):
                aptno = item['aptno']
                floorsno = item['floorsno']
                senduser_id = item['id']
                property_id = item['property_id']

                post_id = ''
                if "post_id" in item.keys():
                    if (item['post_id']):
                        post_id = item['post_id']

                conv_subject = ''
                if "conv_subject" in item.keys():
                    if (item['conv_subject']):
                        conv_subject = item['conv_subject']

                """ if (item['role'] == 'Management'):
                    if (UserTypeID==1):
                        msg_type = 'Tenant'
                    else:
                        msg_type = 'Staff'
                else:
                    msg_type = item['role'] """

                if ((item['role'] == 'Management') or (item['role'] == 'Staff')):
                    msg_type = 'Management'
                else:
                    msg_type = item['role']

                lease_code = item['user_code']
                
                ExistsData = db.session.query(Conversion.id).filter(Conversion.conv_type==conv_type).filter(Conversion.msg_type==msg_type).filter(Conversion.property_id==property_id).filter(Conversion.floorsno==floorsno).filter(Conversion.aptno==aptno).filter(Conversion.lease_code==lease_code).filter(Conversion.post_id==post_id).filter(Conversion.conv_subject==conv_subject).all()

                if not ExistsData:
                    dataInsert = Conversion(
                                        msg_type=msg_type,
                                        conv_type=conv_type,
                                        post_id=post_id, 
                                        conv_subject=conv_subject, 
                                        conv_active='1',
                                        property_id=property_id, 
                                        floorsno=floorsno, 
                                        aptno=aptno, 
                                        lease_code=lease_code)
                    db.session.add(dataInsert)
                    db.session.commit()

                    InsertID = dataInsert.id

                    for sID in sendID:
                        userInsert = Conversionuser(
                                            msg_id=InsertID,
                                            user_id=sID)
                        db.session.add(userInsert)
                        db.session.commit()

                else:
                    insertNew = True
                    for MsgID in ExistsData:
                        ExistsUserData = db.session.query(Conversionuser.user_id).filter(Conversionuser.msg_id==MsgID['id']).all()
                        ExistsID = []
                        for item in ExistsUserData:
                            ExistsID.append(item['user_id'])

                        ExistsID.sort()

                        if (sendID == ExistsID):
                            insertNew = False
                            InsertID = MsgID['id']

                    if (insertNew):
                        dataInsert = Conversion(
                                            msg_type=msg_type,
                                            post_id=post_id, 
                                            conv_subject=conv_subject, 
                                            conv_active='1',
                                            conv_type=conv_type,
                                            property_id=property_id, 
                                            floorsno=floorsno, 
                                            aptno=aptno, 
                                            lease_code=lease_code)
                        db.session.add(dataInsert)
                        db.session.commit()

                        InsertID = dataInsert.id

                        for sID in sendID:
                            userInsert = Conversionuser(
                                                msg_id=InsertID,
                                                user_id=sID)
                            db.session.add(userInsert)
                            db.session.commit()
    else :
        insertNew = True
        conv_type = 'Group'
        msg_type = 'Management'
        
        ExistsData = db.session.query(Conversion.id).filter(Conversion.conv_type==conv_type).filter(Conversion.msg_type==msg_type).filter(Conversion.property_id=='').filter(Conversion.floorsno=='').filter(Conversion.aptno=='').filter(Conversion.lease_code=='').all()
        if not ExistsData:
            dataInsert = Conversion(
                                msg_type=msg_type,
                                conv_type=conv_type,
                                conv_active='1',
                                property_id='', 
                                floorsno='', 
                                aptno='', 
                                lease_code='')
            db.session.add(dataInsert)
            db.session.commit()

            InsertID = dataInsert.id

            for sID in sendID:
                userInsert = Conversionuser(
                                    msg_id=InsertID,
                                    user_id=sID)
                db.session.add(userInsert)
                db.session.commit()

        else:
            for MsgID in ExistsData:
                ExistsUserData = db.session.query(Conversionuser.user_id).filter(Conversionuser.msg_id==MsgID['id']).all()
                ExistsID = []
                for item in ExistsUserData:
                    ExistsID.append(item['user_id'])

                ExistsID.sort()

                if (sendID == ExistsID):
                    insertNew = False
                    InsertID = MsgID['id']
            
            if (insertNew):
                dataInsert = Conversion(
                                    msg_type=msg_type,
                                    conv_type=conv_type,
                                    conv_active='1',
                                    property_id='', 
                                    floorsno='', 
                                    aptno='', 
                                    lease_code='')
                db.session.add(dataInsert)
                db.session.commit()

                InsertID = dataInsert.id

                for sID in sendID:
                    userInsert = Conversionuser(
                                        msg_id=InsertID,
                                        user_id=sID)
                    db.session.add(userInsert)
                    db.session.commit()

    return jsonify({
        'current_user': current_user,
        'SearchSelected': SearchSelected,
        "InsertID": InsertID,
        "sendID": sendID,
        #"ExistsID": ExistsID,
    }), HTTP_201_CREATED
        
@messaging.get("/alldatabyuser/<int:userid>/<int:usertypeid>")
@jwt_required()
def get_alldata(userid, usertypeid):

    selectQ = [Conversion.id, Conversionmsg.user_id, Conversion.aptno, User.firstname, User.lastname, func.max(Conversionmsg.id), Conversionmsg.custom_message, Conversionmsg.created_at]

    MsgData = db.session.query(*selectQ).join(Conversionmsg, Conversion.id == Conversionmsg.msg_id).join(Conversionuser, Conversion.id == Conversionuser.msg_id).join(User, Conversionmsg.user_id == User.id).filter(Conversionuser.user_id==userid).filter(Conversion.conv_active=='1').filter(Conversion.conv_type=='Single').order_by(Conversion.id.desc()).group_by(Conversion.id).all()

    retAllMsgData = []
    for item in MsgData:
        if (usertypeid==1): #Tenant
            CUData = db.session.query(Conversionuser.user_id).join(User, Conversionuser.user_id == User.id).filter(User.usertype_id=='1').filter(Conversionuser.msg_id==item.id).filter(Conversionuser.user_id!=userid).first()
            if CUData:
                new_link = {
                    'id': item.id,
                    'aptno': item.aptno,
                    'senduser_name': item.firstname + " " + item.lastname,
                    'custom_message': item.custom_message,
                    'created_date': (item.created_at).strftime('%b, %d %Y'),
                }

                retAllMsgData.append(new_link)
        elif (usertypeid==2): #Management
            CUData = db.session.query(Conversionuser.user_id).join(User, Conversionuser.user_id == User.id).filter(User.usertype_id=='1').filter(Conversionuser.msg_id==item.id).filter(Conversionuser.user_id!=userid).first()
            if CUData:
                new_link = {
                    'id': item.id,
                    'aptno': item.aptno,
                    'senduser_name': item.firstname + " " + item.lastname,
                    'custom_message': item.custom_message,
                    'created_date': (item.created_at).strftime('%b, %d %Y'),
                }

                retAllMsgData.append(new_link)
        elif (usertypeid==3): #Staff
            new_link = {
                'id': item.id,
                'aptno': item.aptno,
                'senduser_name': item.firstname + " " + item.lastname,
                'custom_message': item.custom_message,
                'created_date': (item.created_at).strftime('%b, %d %Y'),
            }

            retAllMsgData.append(new_link)
    

    MsgData = db.session.query(*selectQ).join(Conversionmsg, Conversion.id == Conversionmsg.msg_id).join(Conversionuser, Conversion.id == Conversionuser.msg_id).join(User, Conversionmsg.user_id == User.id).filter(Conversionuser.user_id==userid).filter(Conversion.conv_active=='1').filter(Conversion.conv_type=='Single').order_by(Conversion.id.desc()).group_by(Conversion.id).all()
 
    retUserMsgData = []
    for item in MsgData:
        if (usertypeid==1): #Tenant
            CUData = db.session.query(Conversionuser.user_id).join(User, Conversionuser.user_id == User.id).filter(User.usertype_id!='1').filter(Conversionuser.msg_id==item.id).filter(Conversionuser.user_id!=userid).first()
            if CUData:
                new_link = {
                    'id': item.id,
                    'senduser_name': item.firstname + " " + item.lastname,
                    'custom_message': item.custom_message,
                    'created_date': (item.created_at).strftime('%b, %d %Y'),
                }

                retUserMsgData.append(new_link)
        elif (usertypeid==2): #Management
            CUData = db.session.query(Conversionuser.user_id).join(User, Conversionuser.user_id == User.id).filter(User.usertype_id.in_([2,3])).filter(Conversionuser.msg_id==item.id).filter(Conversionuser.user_id!=userid).first()
            if CUData:
                new_link = {
                    'id': item.id,
                    'senduser_name': item.firstname + " " + item.lastname,
                    'custom_message': item.custom_message,
                    'created_date': (item.created_at).strftime('%b, %d %Y'),
                }

                retUserMsgData.append(new_link)

        elif (usertypeid==3): #Staff
            new_link = {
                'id': item.id,
                'senduser_name': item.firstname + " " + item.lastname,
                'custom_message': item.custom_message,
                'created_date': (item.created_at).strftime('%b, %d %Y'),
            }

            retUserMsgData.append(new_link)



    MsgData = db.session.query(*selectQ).join(Conversionmsg, Conversion.id == Conversionmsg.msg_id).join(Conversionuser, Conversion.id == Conversionuser.msg_id).join(User, Conversionmsg.user_id == User.id).filter(Conversionuser.user_id==userid).filter(Conversion.conv_active=='1').filter(Conversion.conv_type=='Group').order_by(Conversion.id.desc()).group_by(Conversion.id).all()

    retGroupMsgData = []
    for item in MsgData:
        new_link = {
            'id': item.id,
            'senduser_name': item.firstname + " " + item.lastname,
            'custom_message': item.custom_message,
            'created_date': (item.created_at).strftime('%b, %d %Y'),
        }

        retGroupMsgData.append(new_link)

    return jsonify({
        'retAllMsgData': retAllMsgData,
        'retUserMsgData': retUserMsgData,
        'retGroupMsgData': retGroupMsgData
    }), HTTP_200_OK

@messaging.put("/getchatbyid")
@jwt_required()
def get_getchatbyid():
    ChatID = request.json.get('ChatID', '')
    CurrUserID = request.json.get('CurrUserID', '')

    unReadData = Conversionmsg.query.join(Conversionuser, Conversionmsg.msg_id == Conversionuser.msg_id).filter(Conversionmsg.user_id!=CurrUserID).filter(Conversionuser.user_id==CurrUserID).filter(Conversionmsg.msg_read=='0').filter(Conversionmsg.msg_id==ChatID).all()
    for item in unReadData:
        msgData = Conversionmsg.query.filter_by(msg_id=ChatID, id=item.id).first()
        msgData.msg_read = '1'
        db.session.commit()



    columns = [Conversionmsg.user_id, Conversionmsg.custom_message, Conversionmsg.created_at, User.firstname, User.lastname, User.phoneno]
    MsgData = db.session.query(Conversionmsg).with_entities(*columns).join(User, Conversionmsg.user_id == User.id).filter(Conversionmsg.msg_id==ChatID).order_by(Conversionmsg.id).all()

    retChatData = []

    chat_date = ''
    chat_data = []
    for item in MsgData:
        if (chat_date!=(item.created_at).strftime('%Y-%m-%d')):
            if (chat_date!=''):
                new_link = {
                    'chat_date': chat_date,
                    'chat_data': chat_data,
                }
                retChatData.append(new_link)
                chat_date = ''
                chat_data = []

            chat_date = (item.created_at).strftime('%Y-%m-%d')
            chatdata = {
                'user_id': item.user_id,
                'firstname': item.firstname,
                'lastname': item.lastname,
                'custom_message': item.custom_message,
                'created_date_full': (item.created_at).strftime('%b, %d %Y %I:%M %p'),
                'created_date': (item.created_at).strftime('%I:%M %p'),
            }
            chat_data.append(chatdata)
        else:
            chatdata = {
                'user_id': item.user_id,
                'firstname': item.firstname,
                'lastname': item.lastname,
                'custom_message': item.custom_message,
                'created_date_full': (item.created_at).strftime('%b, %d %Y %I:%M %p'),
                'created_date': (item.created_at).strftime('%I:%M %p'),
            }
            chat_data.append(chatdata)

    
    if (chat_date!=''):
        new_link = {
            'chat_date': chat_date,
            'chat_data': chat_data,
        }
        retChatData.append(new_link)


    UserData = db.session.query(Conversionuser.user_id, User.firstname, User.lastname, User.phoneno, Usertype.usertype).join(User, User.id == Conversionuser.user_id).join(Usertype, Usertype.id == User.usertype_id).filter(Conversionuser.msg_id==ChatID).order_by(Conversionuser.id).all()
    retUserData = []
    for item in UserData:
        retData = {
            'user_id': item.user_id,
            'firstname': item.firstname,
            'lastname': item.lastname,
            'phoneno': item.phoneno,
            'user_type': item.usertype,
        }
        retUserData.append(retData)

    mainChatData = Conversion.query.filter_by(id=ChatID).first()
    retConvSub = ''
    if (mainChatData.conv_subject):
        retConvSub = mainChatData.conv_subject

    return jsonify({
        'retChatData': retChatData,
        'retUserData': retUserData,
        'retConvSub': retConvSub,
    }), HTTP_200_OK

@messaging.get("/unreadcnt/<int:userid>")
@jwt_required()
def get_unreadcnt(userid):

    cntData = Conversionmsg.query.join(Conversionuser, Conversionmsg.msg_id == Conversionuser.msg_id).filter(Conversionmsg.user_id!=userid).filter(Conversionuser.user_id==userid).filter(Conversionmsg.msg_read=='0').count()

    return jsonify({
        'cntData': cntData,
    }), HTTP_200_OK

