#from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

#app = Flask(__name__)

# The same can be done this way:
# app.config['SQLALCHEMY_DATABASE_URI'] = your DB connection string
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
#app.config.from_pyfile('config.py')

# pass in the app into SQLAlchemy to create a db session.
#db = SQLAlchemy(app)

db = SQLAlchemy()

class Siteconfigs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    site_text = db.Column(db.String(100), nullable=False)
    site_value = db.Column(db.String(100), nullable=False)

    def __repr__(self) -> str:
        return 'Siteconfigs>>> {self.id}'

class Tenantconfigs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tenants_post_max_month = db.Column(db.Integer, default=5)
    tenants_post_max_modify = db.Column(db.Integer, default=3)
    tenants_post_expire_month = db.Column(db.Integer, default=6)
    tenants_post_add_cost = db.Column(db.Float, default=0.80)
    tenants_post_add_no = db.Column(db.Integer, default=2)
    tenants_post_image = db.Column(db.Integer, default=4)
    tenants_conversation_delete_month = db.Column(db.Integer, default=4)

    #if not (db.engine.has_table('tenantconfigs')):
    #    db.create_all()

    #db.session.add(Tenantconfigs(tenants_post_max_month=5, tenants_post_expire_month=6, tenants_post_add_cost=0.80, tenants_post_add_no=2, tenants_post_image=4, tenants_conversation_delete_month=4))
    #db.session.commit()

    #def __init__(self):
    #    self.tenants_post_max_month = 5
    #    self.tenants_post_expire_month = 6
    #    self.tenants_post_add_cost = 0.80
    #    self.tenants_post_add_no = 2
    #    self.tenants_post_image = 4
    #    self.tenants_conversation_delete_month = 4

    def __repr__(self) -> str:
        return 'Tenantconfigs>>> {self.id}'

class Usertype(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    usertype = db.Column(db.String(20), nullable=False)
    usertype_desc = db.Column(db.String(100))
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Usertype>>> {self.id}'

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    usertype_id = db.Column(db.Integer, db.ForeignKey('usertype.id'))
    user_active = db.Column(db.Integer, default=0)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.Text(), nullable=False)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    phoneno = db.Column(db.String(25), nullable=False)
    reset_code = db.Column(db.String(5), nullable=False)
    stripe_api_key = db.Column(db.String(100), nullable=False)
    comm_percentage = db.Column(db.Float(), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())
    propertys = db.relationship('Propertydata', backref="user")

    def __repr__(self) -> str:
        return 'User>>> {self.id}'

class Userpaymethod(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    stripe_pay_method_id = db.Column(db.String(50), nullable=False)
    fernet_key = db.Column(db.Text(), nullable=False)
    stripe_token_data = db.Column(db.Text(), nullable=False)
    pay_name = db.Column(db.String(50), nullable=False)
    default_method = db.Column(db.Integer)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Userpaymethod>>> {self.id}'

class Usercode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user_code = db.Column(db.String(15), nullable=False)
    user_prop_id = db.Column(db.Integer, default=0)
    user_prop_name = db.Column(db.String(80), nullable=True)
    user_prop_suffix = db.Column(db.String(3), nullable=True)
    user_active_code = db.Column(db.Integer, default=1)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Usercode>>> {self.id}'

class Accessdata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    access_code = db.Column(db.String(15), nullable=False)
    assign_flag = db.Column(db.Integer, default=0)
    assign_user_id = db.Column(db.Integer, default=0)
    active_flag = db.Column(db.Integer, default=1)
    feature_access = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Accessdata>>> {self.id}'

class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Country>>> {self.id}'

class State(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    country_id = db.Column(db.Integer, db.ForeignKey('country.id', ondelete='CASCADE', onupdate='CASCADE'))
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'State>>> {self.id}'

class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    state_id = db.Column(db.Integer, db.ForeignKey('state.id', ondelete='CASCADE', onupdate='CASCADE'))
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'City>>> {self.id}'

class Unitformat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    floorsno = db.Column(db.Integer, default=0)
    middlename = db.Column(db.String(5), nullable=False)
    aptno = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Unitformat>>> {self.id}'

class Propertydata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    owner = db.Column(db.String(80), nullable=True)
    suffix = db.Column(db.String(3), nullable=False)
    address = db.Column(db.Text, nullable=True)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id', ondelete='CASCADE', onupdate='CASCADE'))
    state_id = db.Column(db.Integer, db.ForeignKey('state.id', ondelete='CASCADE', onupdate='CASCADE'))
    country_id = db.Column(db.Integer, db.ForeignKey('country.id', ondelete='CASCADE', onupdate='CASCADE'))
    postalcode = db.Column(db.String(25), nullable=True)
    nooffloors = db.Column(db.Integer, default=0)
    noofaptperfloors = db.Column(db.Integer, default=0)
    unitformat_id = db.Column(db.Integer, db.ForeignKey('unitformat.id', ondelete='CASCADE', onupdate='CASCADE'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user_current_manage = db.Column(db.Integer, default=0)
    remove_flag = db.Column(db.Integer, default=0)
    sorting_id = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Propertydata>>> {self.id}'

class Propertyfloordata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    property_id = db.Column(db.Integer, db.ForeignKey('propertydata.id'))
    floorsno = db.Column(db.Integer, default=0)
    aptno = db.Column(db.String(15), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Propertyfloordata>>> {self.id}'

class Propertyleases(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    property_id = db.Column(db.Integer)
    floorsno = db.Column(db.Integer)
    aptno = db.Column(db.String(15))
    file_storagekey = db.Column(db.String(15), index = True)
    lease_code = db.Column(db.String(15), nullable=False)
    lease_price = db.Column(db.Float)
    lease_start_date = db.Column(db.Date)
    lease_end_date = db.Column(db.Date)
    lease_cancel = db.Column(db.Integer, default=0)
    lease_propose_renewal = db.Column(db.Integer, default=0)
    lease_notification_send = db.Column(db.Integer, default=0, nullable=True)
    rent_notification_send = db.Column(db.Integer, default=0, nullable=True)
    lease_payment_flag = db.Column(db.String(10), default='Pending')
    lease_payment_date = db.Column(db.Date)
    lease_stripe_charge_id = db.Column(db.String(50), nullable=False)
    lease_price_pay = db.Column(db.Float, default=0)
    billing_flag_update = db.Column(db.Integer, default=0, nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Propertyleases>>> {self.id}'

class Propertyleasestenant(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    leases_id = db.Column(db.Integer, db.ForeignKey('propertyleases.id'))
    tenant_fname = db.Column(db.String(50), nullable=False)
    tenant_lname = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Propertyleasestenant>>> {self.id}'

class Propertyleasesfile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index = True)
    leases_id = db.Column(db.Integer, db.ForeignKey('propertyleases.id'), index = True)
    file_name = db.Column(db.String(255), index = True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Propertyleasesfile>>> {self.id}'

class Billinghistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    billing_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    billing_date = db.Column(db.Date)
    billing_date_text = db.Column(db.String(20), index = True)
    billing_cost = db.Column(db.Float, default=0)
    billing_lease_cnt = db.Column(db.Integer, default=0, nullable=True)
    billing_status = db.Column(db.String(20), index = True)
    billing_payment_flag = db.Column(db.String(10), default='Pending')
    billing_payment_date = db.Column(db.Date)
    billing_payment_amount = db.Column(db.Float, default=0)
    billing_stripe_charge_id = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Billinghistory>>> {self.id}'

class Billinghistorysub(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    billing_id = db.Column(db.Integer, db.ForeignKey('billinghistory.id'))
    billing_leases_id = db.Column(db.Integer, db.ForeignKey('propertyleases.id'))
    billing_lease_price = db.Column(db.Float, default=0)
    billing_comm_per = db.Column(db.Float(), nullable=False)
    billing_comm_amt = db.Column(db.Float(), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Billinghistorysub>>> {self.id}'

class Bonuspost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bonuspost_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    bonuspost_date = db.Column(db.Date)
    bonuspost_date_text = db.Column(db.String(20), index = True)
    bonuspost_pay = db.Column(db.Float, default=0)
    bonuspost_cnt = db.Column(db.Integer, default=0, nullable=True)
    bonuspost_payment_flag = db.Column(db.String(10), default='Paid')
    bonuspost_payment_date = db.Column(db.Date)
    bonuspost_payment_amount = db.Column(db.Float, default=0)
    bonuspost_stripe_charge_id = db.Column(db.String(50), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Bonusposts>>> {self.id}'

class Postdata(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_title = db.Column(db.String(100), nullable=False)
    post_message = db.Column(db.Text, nullable=True)
    post_storagekey = db.Column(db.String(15), index = True)
    post_activation_date = db.Column(db.Date)
    post_expiration_date = db.Column(db.Date)
    post_expire = db.Column(db.Integer, default=0)
    post_pinned_flag = db.Column(db.Integer, default=0)
    post_date = db.Column(db.Date)
    post_price = db.Column(db.Float, nullable=True)
    post_updated_cnt = db.Column(db.Integer, default=0)
    post_prop_id = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Postdata>>> {self.id}'

class Postdatafile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('postdata.id'), index = True)
    file_name = db.Column(db.String(255), index = True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Postdatafile>>> {self.id}'

class Faq(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    usertype_id = db.Column(db.Integer, db.ForeignKey('usertype.id'))
    faq_title = db.Column(db.String(100), nullable=False)
    faq_message = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Faq>>> {self.id}'

class Conversion(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    msg_type = db.Column(db.String(10), nullable=False)
    conv_type = db.Column(db.String(10), nullable=False)
    property_id = db.Column(db.Integer, nullable=True)
    floorsno = db.Column(db.Integer, nullable=True)
    aptno = db.Column(db.String(15), nullable=True)
    lease_code = db.Column(db.String(15), nullable=True)
    post_id = db.Column(db.Integer, nullable=True)
    conv_subject = db.Column(db.String(100), nullable=True)
    conv_active = db.Column(db.Integer, nullable=True)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Conversion>>> {self.id}'

class Conversionuser(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    msg_id = db.Column(db.Integer, db.ForeignKey('conversion.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Conversionuser>>> {self.id}'

class Conversionmsg(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    msg_id = db.Column(db.Integer, db.ForeignKey('conversion.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    custom_message = db.Column(db.Text, nullable=False)
    msg_read = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Conversionmsg>>> {self.id}'

class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    n_type = db.Column(db.String(10), index = True)
    n_date = db.Column(db.Date)
    n_property_id = db.Column(db.Integer)
    n_lease_code = db.Column(db.String(15), nullable=False)
    n_title = db.Column(db.String(15), nullable=False)
    n_subtitle = db.Column(db.String(15), nullable=False)
    n_desc = db.Column(db.String(100), nullable=False)
    n_link = db.Column(db.String(50), nullable=False)
    n_read = db.Column(db.Integer, default=0)
    n_clear = db.Column(db.Integer, default=0)
    created_at = db.Column(db.DateTime, default=datetime.now())
    updated_at = db.Column(db.DateTime, onupdate=datetime.now())

    def __repr__(self) -> str:
        return 'Notification>>> {self.id}'

""" if __name__ == '__main__':
    # The creation starts here.
    # the same can be done by creating an create_engine object.
    # example:
    # from sqlalchemy import create_engine
    # engine = create_engine()
    # if not engine.has_table(tablename)
    if not (db.engine.has_table('tenantconfigs')):
        db.create_all()
 
    # inserting new information about tenantconfigs
    db.session.add(Tenantconfigs(tenants_post_max_month=5, tenants_post_expire_month=6, tenants_post_add_cost=0.80, tenants_post_add_no=2, tenants_post_image=4, tenants_conversation_delete_month=4))
    db.session.commit()
 """

#db.create_all()

#db.session.add(Tenantconfigs(tenants_post_max_month=5, tenants_post_expire_month=6, tenants_post_add_cost=0.80, tenants_post_add_no=2, tenants_post_image=4, tenants_conversation_delete_month=4))
#db.session.commit()
