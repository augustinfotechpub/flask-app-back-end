from sqlalchemy import true
from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import City, State, Country, db, Usertype, Tenantconfigs, Siteconfigs

setting = Blueprint("setting", __name__, url_prefix="/api/v1/setting")

@setting.route('/city/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_city():

    if request.method == 'POST':
        name = request.get_json().get('name', '')
        state_id = request.get_json().get('state_id', '')

        cityAdd = City(name=name, state_id=state_id)
        db.session.add(cityAdd)
        db.session.commit()

        return jsonify({
            'id': cityAdd.id,
            'name': cityAdd.name,
            'state_id': cityAdd.state_id,
            'created_at': cityAdd.created_at,
            'updated_at': cityAdd.updated_at,
        }), HTTP_201_CREATED

    elif request.method == 'DELETE':
        id = request.get_json().get('id', '')
        cityData = City.query.filter_by(id=id).first()

        if not cityData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        db.session.delete(cityData)
        db.session.commit()

        return jsonify({
            'deleteFlag': True,
            'id': id,
        }), HTTP_200_OK

    else:
        id = request.get_json().get('id', '')
        name = request.get_json().get('name', '')
        state_id = request.get_json().get('state_id', '')

        cityData = City.query.filter_by(id=id).first()

        if not cityData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        cityData.name = name
        cityData.state_id = state_id
        db.session.commit()

        return jsonify({
            'id': cityData.id,
            'name': cityData.name,
            'state_id': cityData.state_id,
            'created_at': cityData.created_at,
            'updated_at': cityData.updated_at,
        }), HTTP_201_CREATED

@setting.get("/city/<int:id>")
@jwt_required()
def get_city(id):
    #cityData = City.query.filter_by(id=id).first()
    cityData = db.session.query(City.id, City.name, City.state_id, State.country_id).join(State, State.id == City.state_id).filter(City.id==id).first()

    if not cityData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'cityID': cityData.id,
        'cityName': cityData.name,
        'stateID': cityData.state_id,
        'countryID': cityData.country_id,
    }), HTTP_200_OK

@setting.route('/state/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_state():

    if request.method == 'POST':
        name = request.get_json().get('name', '')
        country_id = request.get_json().get('country_id', '')

        stateAdd = State(name=name, country_id=country_id)
        db.session.add(stateAdd)
        db.session.commit()

        return jsonify({
            'id': stateAdd.id,
            'name': stateAdd.name,
            'country_id': stateAdd.country_id,
            'created_at': stateAdd.created_at,
            'updated_at': stateAdd.updated_at,
        }), HTTP_201_CREATED

    elif request.method == 'DELETE':
        id = request.get_json().get('id', '')
        stateData = State.query.filter_by(id=id).first()

        if not stateData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        db.session.delete(stateData)
        db.session.commit()

        return jsonify({
            'deleteFlag': True,
            'id': id,
        }), HTTP_200_OK

    else:
        id = request.get_json().get('id', '')
        name = request.get_json().get('name', '')
        country_id = request.get_json().get('country_id', '')

        stateData = State.query.filter_by(id=id).first()

        if not stateData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        stateData.name = name
        stateData.country_id = country_id
        db.session.commit()

        return jsonify({
            'id': stateData.id,
            'name': stateData.name,
            'country_id': stateData.country_id,
            'created_at': stateData.created_at,
            'updated_at': stateData.updated_at,
        }), HTTP_201_CREATED

@setting.get("/state/<int:id>")
@jwt_required()
def get_state(id):
    stateData = State.query.filter_by(id=id).first()

    if not stateData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'stateID': stateData.id,
        'countryID': stateData.country_id,
        'stateName': stateData.name,
    }), HTTP_200_OK

@setting.route('/country/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_country():

    if request.method == 'POST':
        name = request.get_json().get('name', '')

        countryAdd = Country(name=name)
        db.session.add(countryAdd)
        db.session.commit()

        return jsonify({
            'id': countryAdd.id,
            'name': countryAdd.name,
            'created_at': countryAdd.created_at,
            'updated_at': countryAdd.updated_at,
        }), HTTP_201_CREATED

    elif request.method == 'DELETE':
        id = request.get_json().get('id', '')
        countryData = Country.query.filter_by(id=id).first()

        if not countryData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        db.session.delete(countryData)
        db.session.commit()

        return jsonify({
            'deleteFlag': True,
            'id': id,
        }), HTTP_200_OK

    else:
        id = request.get_json().get('id', '')
        name = request.get_json().get('name', '')

        countryData = Country.query.filter_by(id=id).first()

        if not countryData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        countryData.name = name
        db.session.commit()

        return jsonify({
            'id': countryData.id,
            'name': countryData.name,
            'created_at': countryData.created_at,
            'updated_at': countryData.updated_at,
        }), HTTP_201_CREATED

@setting.get("/country/<int:id>")
@jwt_required()
def get_country(id):
    countryData = Country.query.filter_by(id=id).first()

    if not countryData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'countryID': countryData.id,
        'countryName': countryData.name,
    }), HTTP_200_OK

@setting.get("/usertype/<int:id>")
@jwt_required()
def get_usertype(id):
    usertypeData = Usertype.query.filter_by(id=id).first()

    if not usertypeData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'usertypeID': usertypeData.id,
        'usertypeName': usertypeData.usertype,
        'usertypeDesc': usertypeData.usertype_desc,
    }), HTTP_200_OK

@setting.route('/usertype/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_usertype():

    if request.method == 'POST':
        usertype_desc = request.get_json().get('usertype_desc', '')

        usertypeAdd = Usertype(usertype_desc=usertype_desc)
        db.session.add(usertypeAdd)
        db.session.commit()

        return jsonify({
            'id': usertypeAdd.id,
            'usertype': usertypeAdd.usertype,
            'usertype_desc': usertypeAdd.usertype_desc,
        }), HTTP_201_CREATED

    else:
        id = request.get_json().get('id', '')
        usertype_desc = request.get_json().get('usertype_desc', '')

        usertypeData = Usertype.query.filter_by(id=id).first()

        if not usertypeData:
            return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

        usertypeData.usertype_desc = usertype_desc
        db.session.commit()

        return jsonify({
            'id': usertypeData.id,
            'usertype': usertypeData.usertype,
            'usertype_desc': usertypeData.usertype_desc,
        }), HTTP_201_CREATED

@setting.get("/tenantconfigs/<int:id>")
@jwt_required()
def get_tenantconfigs(id):
    TenantconfigsData = Tenantconfigs.query.filter_by(id=id).first()

    if not TenantconfigsData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    return jsonify({
        'tenants_post_max_month': TenantconfigsData.tenants_post_max_month,
        'tenants_post_max_modify': TenantconfigsData.tenants_post_max_modify,
        'tenants_post_expire_month': TenantconfigsData.tenants_post_expire_month,
        'tenants_post_add_cost': TenantconfigsData.tenants_post_add_cost,
        'tenants_post_add_no': TenantconfigsData.tenants_post_add_no,
        'tenants_post_image': TenantconfigsData.tenants_post_image,
        'tenants_conversation_delete_month': TenantconfigsData.tenants_conversation_delete_month,
    }), HTTP_200_OK

@setting.route('/tenantconfigs/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_tenantconfigs():

    if request.method == 'PUT':
        id = request.get_json().get('id', '')
        tenants_post_max_month = request.get_json().get('tenants_post_max_month', '')
        tenants_post_max_modify = request.get_json().get('tenants_post_max_modify', '')
        tenants_post_expire_month = request.get_json().get('tenants_post_expire_month', '')
        tenants_post_add_cost = request.get_json().get('tenants_post_add_cost', '')
        tenants_post_add_no = request.get_json().get('tenants_post_add_no', '')
        tenants_post_image = request.get_json().get('tenants_post_image', '')
        tenants_conversation_delete_month = request.get_json().get('tenants_conversation_delete_month', '')

        TenantconfigsData = Tenantconfigs.query.filter_by(id=id).first()

        if not TenantconfigsData:
            TenantconfigsDataAdd = Tenantconfigs(
                            tenants_post_max_month=tenants_post_max_month, 
                            tenants_post_max_modify=tenants_post_max_modify, 
                            tenants_post_expire_month=tenants_post_expire_month, 
                            tenants_post_add_cost=tenants_post_add_cost, 
                            tenants_post_add_no=tenants_post_add_no, 
                            tenants_post_image=tenants_post_image, 
                            tenants_conversation_delete_month=tenants_conversation_delete_month)
            db.session.add(TenantconfigsDataAdd)
            db.session.commit()

            TenantconfigsData_id = TenantconfigsDataAdd.id
        else:
            TenantconfigsData.tenants_post_max_month = tenants_post_max_month
            TenantconfigsData.tenants_post_max_modify = tenants_post_max_modify
            TenantconfigsData.tenants_post_expire_month = tenants_post_expire_month
            TenantconfigsData.tenants_post_add_cost = tenants_post_add_cost
            TenantconfigsData.tenants_post_add_no = tenants_post_add_no
            TenantconfigsData.tenants_post_image = tenants_post_image
            TenantconfigsData.tenants_conversation_delete_month = tenants_conversation_delete_month
            db.session.commit()

            TenantconfigsData_id = TenantconfigsData.id

        return jsonify({
            'id': TenantconfigsData_id,
        }), HTTP_201_CREATED

@setting.route('/siteconfigs/', methods=['POST', 'GET', 'PUT', 'PATCH', 'DELETE'])
@jwt_required()
def handle_siteconfig():

    if request.method == 'POST':
        site_text = request.get_json().get('site_text', '')
        site_value = request.get_json().get('site_value', '')

        siteConfigData = Siteconfigs.query.filter_by(site_text=site_text).first()
        if siteConfigData:
            siteConfigData.site_value = site_value
            db.session.commit()
        else:
            siteConfigAdd = Siteconfigs(site_text=site_text, site_value=site_value)
            db.session.add(siteConfigAdd)
            db.session.commit()

        return jsonify({
            'site_text': site_text,
        }), HTTP_201_CREATED

    else:
        data = {}
        items = Siteconfigs.query.filter_by().all()
        for item in items:
            data.update({item.site_text : item.site_value})

        return jsonify(data), HTTP_200_OK

