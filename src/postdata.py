from src.constants.http_status_codes import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_409_CONFLICT
from flask import Blueprint, request
from flask.json import jsonify
from flask_jwt_extended import jwt_required, get_jwt
from src.database import Postdata, Postdatafile, db, User, Propertydata, Tenantconfigs, Conversion, Userpaymethod, Bonuspost, Siteconfigs
from datetime import date, datetime, timedelta
import datetime
from dateutil import relativedelta
from sqlalchemy import extract, false
from sqlalchemy import and_
from sqlalchemy import or_
import stripe
import json
from cryptography.fernet import Fernet
from sqlalchemy.sql import func

postdata = Blueprint("postdata", __name__, url_prefix="/api/v1/postdata")

@postdata.route('/', methods=['POST', 'GET'])
@jwt_required()
def addPostData():
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = request.get_json().get('current_user', '')
    
    user_type_id = request.get_json().get('user_type_id', '')

    tenants_max_post = 1
    tenants_post_cost = 1
    tenants_post_no = 1
    TenantConfigsData = Tenantconfigs.query.filter_by(id=1).first()
    if TenantConfigsData:
        tenants_max_post = TenantConfigsData.tenants_post_max_month
        tenants_post_cost = TenantConfigsData.tenants_post_add_cost
        tenants_post_no = TenantConfigsData.tenants_post_add_no



    InsertFlag = True
    if (user_type_id=='1'):
        currentMonth = int(request.get_json().get('poMonth', '')) #date.today().month
        PostsCnt = db.session.query(Postdata).filter(Postdata.user_id == current_user).filter(extract('month', Postdata.post_date) == currentMonth).count()

        BonusPostsData = db.session.query(func.sum(Bonuspost.bonuspost_cnt).label('totcnt')).filter(Bonuspost.bonuspost_user_id == current_user).filter(extract('month', Bonuspost.bonuspost_date) == currentMonth).all()
        BonusCnt = 0
        for item in BonusPostsData:
            if (item.totcnt):
                BonusCnt = BonusCnt + item.totcnt
            else:
                BonusCnt = BonusCnt
        
        tenants_max_post = tenants_max_post + BonusCnt

        if (PostsCnt>=tenants_max_post):
            InsertFlag = False

    post_title = request.get_json().get('post_title', '')
    post_message = request.get_json().get('post_message', '')
    post_storagekey = request.get_json().get('post_storagekey', '')
    post_price = request.get_json().get('post_price', 0)
    post_pinned = request.get_json().get('post_pinned_flag', '')
    if (post_pinned):
        post_pinned_flag = 1
    else:
        post_pinned_flag = 0
    
    if (post_price==''):
        post_price = 0

    aYear = request.get_json().get('aYear', '')
    aMonth = request.get_json().get('aMonth', '')
    aDate = request.get_json().get('aDate', '') 
    post_activation_date = datetime.datetime(int(aYear), int(aMonth), int(aDate))

    eYear = request.get_json().get('eYear', '')
    eMonth = request.get_json().get('eMonth', '')
    eDate = request.get_json().get('eDate', '') 
    post_expiration_date = datetime.datetime(int(eYear), int(eMonth), int(eDate))

    poYear = request.get_json().get('poYear', '')
    poMonth = request.get_json().get('poMonth', '')
    poDate = request.get_json().get('poDate', '') 
    post_date = datetime.datetime(int(poYear), int(poMonth), int(poDate))

    post_prop_id = request.get_json().get('post_prop_id', '')
    #post_date = date.today()

    if (InsertFlag) :
        dataInsert = Postdata(
                            post_title=post_title, 
                            post_prop_id=post_prop_id,
                            post_message=post_message, 
                            post_price=float(post_price), 
                            post_storagekey=post_storagekey, 
                            post_pinned_flag=post_pinned_flag,
                            post_activation_date=post_activation_date,
                            post_expiration_date=post_expiration_date, 
                            post_expire='0',
                            post_date=post_date, 
                            user_id=current_user)
        db.session.add(dataInsert)
        db.session.commit()

        filesData = request.get_json().get('fileData', '')
        for fData in filesData:
            subDataInsert = Postdatafile(
                                post_id=dataInsert.id, 
                                file_name=fData['filepath'])
            db.session.add(subDataInsert)
            db.session.commit()

        return jsonify({
            'id': dataInsert.id,
            'post_title': dataInsert.post_title,
            'InsertFlag': InsertFlag,
        }), HTTP_201_CREATED
    else:
        return jsonify({
            'InsertFlag': InsertFlag,
            'tenants_max_post': tenants_max_post,
            'tenants_post_cost': tenants_post_cost,
            'tenants_post_no': tenants_post_no,
            'currentMonth': currentMonth,
            'PostsCnt': PostsCnt,
        }), HTTP_201_CREATED
        
@postdata.get("/configdata/<int:user_id>")
@jwt_required()
def get_configdata(user_id):

    TenantConfigsData = Tenantconfigs.query.filter_by(id=1).first()
    if TenantConfigsData:
        tenants_post_image = TenantConfigsData.tenants_post_image
        tenants_post_max_modify = TenantConfigsData.tenants_post_max_modify
        tenants_max_post = TenantConfigsData.tenants_post_max_month
        tenants_post_cost = TenantConfigsData.tenants_post_add_cost
        tenants_post_no = TenantConfigsData.tenants_post_add_no

    PostsCnt = db.session.query(Postdata).filter(Postdata.user_id == user_id).filter(extract('month', Postdata.post_date) == date.today().month).count()
    BonusPostsData = db.session.query(func.sum(Bonuspost.bonuspost_cnt).label('totcnt')).filter(Bonuspost.bonuspost_user_id == user_id).filter(extract('month', Bonuspost.bonuspost_date) == date.today().month).all()
    BonusCnt = 0
    if BonusPostsData:
        for item in BonusPostsData:
            if item.totcnt is not None:
                BonusCnt = BonusCnt + item.totcnt
    
    tenants_max_post = tenants_max_post + BonusCnt

    ManagementConfigsData = Tenantconfigs.query.filter_by(id=2).first()
    if ManagementConfigsData:
        management_post_image = ManagementConfigsData.tenants_post_image

    return jsonify({
        'Tetst1': user_id,
        'Tetst2': date.today().month,

        'management_post_image': management_post_image,
        'tenants_post_image': tenants_post_image,
        'tenants_post_max_modify': tenants_post_max_modify,
        'tenants_max_post': tenants_max_post,
        'tenants_post_cost': tenants_post_cost,
        'tenants_post_no': tenants_post_no,
        'PostsCnt': PostsCnt,
        'BonusCnt': BonusCnt,
    }), HTTP_200_OK

@postdata.get("/alldatabyuser/<int:userid>")
@jwt_required()
def get_alldata(userid):
    GeneralPostsData = []

    if (userid==0):
        PostData = db.session.query(Postdata).filter(Postdata.post_pinned_flag == 0).order_by(Postdata.id.desc()).all()
    else:
        PostData = db.session.query(Postdata).filter(Postdata.post_pinned_flag == 0).filter(Postdata.user_id == userid).order_by(Postdata.id.desc()).all()

    for item in PostData:
        if (item.post_date):
            post_date = (datetime.datetime.strptime(str(item.post_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_activation_date):
            post_activation_date = (datetime.datetime.strptime(str(item.post_activation_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        updated_at = ''
        if (item.updated_at):
            updated_at = (item.updated_at).strftime('%b. %d, %Y')
        else:
            updated_at = (item.created_at).strftime('%b. %d, %Y')

        if (item.post_expiration_date):
            post_expiration_date = (datetime.datetime.strptime(str(item.post_expiration_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        FData = []
        FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        for subitem in FilesData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)

        new_link = {
            'id': item.id,
            'title': item.post_title,
            'filestoragekey': item.post_storagekey,
            'content': item.post_message,
            'date': updated_at,
            'post_expiration_date': post_expiration_date,
            'post_date': post_date,
            'attachFile': FData,
        }

        GeneralPostsData.append(new_link)
    

    PinnedPostsData = []

    if (userid==0):
        PostData = db.session.query(Postdata).filter(Postdata.post_pinned_flag == 1).order_by(Postdata.id.desc()).all()
    else:
        PostData = db.session.query(Postdata).filter(Postdata.post_pinned_flag == 1).filter(Postdata.user_id == userid).order_by(Postdata.id.desc()).all()

    for item in PostData:
        if (item.post_date):
            post_date = (datetime.datetime.strptime(str(item.post_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        updated_at = ''
        if (item.updated_at):
            updated_at = (item.updated_at).strftime('%b. %d, %Y')
        else:
            updated_at = (item.created_at).strftime('%b. %d, %Y')

        if (item.post_activation_date):
            post_activation_date = (datetime.datetime.strptime(str(item.post_activation_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_expiration_date):
            post_expiration_date = (datetime.datetime.strptime(str(item.post_expiration_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        FData = []
        FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        for subitem in FilesData:
            new_add = {
                'file_name': subitem.file_name,
            }
            FData.append(new_add)

        new_link = {
            'id': item.id,
            'title': item.post_title,
            'filestoragekey': item.post_storagekey,
            'content': item.post_message,
            'date': updated_at,
            'post_expiration_date': post_expiration_date,
            'post_date': post_date,
            'attachFile': FData,
        }

        PinnedPostsData.append(new_link)

    return jsonify({
        'PinnedPostsData': PinnedPostsData,
        'GeneralPostsData': GeneralPostsData,
    }), HTTP_200_OK

@postdata.get("/<int:id>/<int:user_id>")
@jwt_required()
def get_infobyid(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    TenantConfigsData = Tenantconfigs.query.filter_by(id=1).first()
    if TenantConfigsData:
        tenants_post_image = TenantConfigsData.tenants_post_image
        tenants_post_max_modify = TenantConfigsData.tenants_post_max_modify

    retData = Postdata.query.filter_by(id=id).first()

    if not retData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    retPostUserData = User.query.filter_by(id=retData.user_id).first()

    FData = []
    FileData = db.session.query(Postdatafile).filter(Postdatafile.post_id == retData.id).all()
    for subitem in FileData:
        new_add = {
            'file_name': subitem.file_name,
        }
        FData.append(new_add)
    
    if (retData.post_date):
        post_date = (datetime.datetime.strptime(str(retData.post_date), "%Y-%m-%d"))

    if (retData.post_activation_date):
        post_activation_date_text = (datetime.datetime.strptime(str(retData.post_activation_date), "%Y-%m-%d")).strftime('%b, %d %Y')

    if (retData.post_expiration_date):
        post_expiration_date = (datetime.datetime.strptime(str(retData.post_expiration_date), "%Y-%m-%d")).strftime('%Y-%m-%d')

    return jsonify({
        'tenants_post_max_modify': tenants_post_max_modify,
        'post_id': retData.id,
        'post_user_id': retData.user_id,
        'post_updated_cnt': retData.post_updated_cnt,
        'post_title': retData.post_title,
        'post_message': retData.post_message,
        'post_storagekey': retData.post_storagekey,
        #'post_activation_date': post_activation_date+"T00:00:00+05:30",#'2022-01-01T13:47:20.789'
        #'post_expiration_date': post_expiration_date+"T00:00:00+05:30",
        'post_price': retData.post_price,
        'post_pinned_flag': retData.post_pinned_flag,
        'post_date': retData.post_date,
        'post_activation_date': retData.post_activation_date,
        'post_activation_date_text': post_activation_date_text,
        'post_expiration_date': retData.post_expiration_date,
        'post_date': post_date,
        'FData': FData,
        'post_user_name': retPostUserData.firstname + " " + retPostUserData.lastname,
    }), HTTP_200_OK

@postdata.put('/<int:id>/<int:user_id>')
@postdata.patch('/<int:id>/<int:user_id>')
@jwt_required()
def editpostdata(id, user_id):
    current_user = 0
    userData = get_jwt()
    current_user = userData['user_id']

    if current_user == 0:
        current_user = user_id

    retData = Postdata.query.filter_by(id=id).first()

    if not retData:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    post_title = request.get_json().get('post_title', '')
    post_message = request.get_json().get('post_message', '')
    post_price = request.get_json().get('post_price', 0)
    post_pinned = request.get_json().get('post_pinned_flag', '')
    if (post_pinned):
        post_pinned_flag = 1
    else:
        post_pinned_flag = 0
    
    if (post_price==''):
        post_price = 0

    aYear = request.get_json().get('aYear', '')
    aMonth = request.get_json().get('aMonth', '')
    aDate = request.get_json().get('aDate', '') 
    post_activation_date = datetime.datetime(int(aYear), int(aMonth), int(aDate))

    eYear = request.get_json().get('eYear', '')
    eMonth = request.get_json().get('eMonth', '')
    eDate = request.get_json().get('eDate', '') 
    post_expiration_date = datetime.datetime(int(eYear), int(eMonth), int(eDate))

    poYear = request.get_json().get('poYear', '')
    poMonth = request.get_json().get('poMonth', '')
    poDate = request.get_json().get('poDate', '') 
    post_date = datetime.datetime(int(poYear), int(poMonth), int(poDate))


    retData.post_title = post_title
    retData.post_message = post_message
    retData.post_activation_date = post_activation_date
    retData.post_expiration_date = post_expiration_date

    retData.post_price = post_price
    retData.post_pinned_flag = post_pinned_flag
    retData.post_date = post_date

    retData.post_updated_cnt = retData.post_updated_cnt + 1
   

    db.session.commit()

    db.session.query(Postdatafile).filter_by(post_id=id).delete(synchronize_session=False)
    db.session.commit()

    filesData = request.get_json().get('fileData', '')
    for fData in filesData:
        subDataInsert = Postdatafile(
                            post_id=id, 
                            file_name=fData['filepath'])
        db.session.add(subDataInsert)
        db.session.commit()

    return jsonify({
        'id': retData.id,
        'post_title': retData.post_title,
        'post_message': retData.post_message,
    }), HTTP_200_OK

@postdata.get("/datafortenant/<int:userid>/<int:limitno>/<int:propID>")
@jwt_required()
def getdatafortenant(userid,limitno, propID):

    currDt = date.today()

    property = db.session.query(Propertydata).filter(Propertydata.id==propID).first()
    MaID = 0
    CityID = 0
    if property:
        MaID = property.user_id
        CityID = property.city_id
        

    GeneralPostsData = []
    """ PostData = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 2).filter(Postdata.post_pinned_flag == 1).order_by(Postdata.id.desc()).limit(4).all()
    GeneralPostsCnt = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 2).filter(Postdata.post_pinned_flag == 1).count()
    for item in PostData:
        if (item.post_date):
            post_date = (datetime.datetime.strptime(str(item.post_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_activation_date):
            post_activation_date = (datetime.datetime.strptime(str(item.post_activation_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_expiration_date):
            post_expiration_date = (datetime.datetime.strptime(str(item.post_expiration_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        FData = []
        FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        for idx, subitem in enumerate(FilesData):
            if (idx==0) :
                new_add = {
                    'file_name': subitem.file_name,
                }
                FData.append(new_add)

        new_link = {
            'id': item.id,
            'title': item.post_title,
            'filestoragekey': item.post_storagekey,
            'content': item.post_message,
            'date': post_activation_date,
            'post_expiration_date': post_expiration_date,
            'post_date': post_date,
            'attachFile': FData,
        }

        GeneralPostsData.append(new_link) """
    
    TenantPostsData = []
    if (limitno==0):
        PostData = db.session.query(Postdata).join(Propertydata, Postdata.post_prop_id == Propertydata.id).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 1).filter(Postdata.post_expiration_date >= currDt).filter(Postdata.post_expire == 0).filter(or_(and_(Propertydata.city_id == CityID, Postdata.post_activation_date <= currDt, Postdata.post_expiration_date >= currDt), Postdata.user_id == userid)).order_by(Postdata.id.desc()).all()
    else:
        PostData = db.session.query(Postdata).join(Propertydata, Postdata.post_prop_id == Propertydata.id).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 1).filter(Postdata.post_expiration_date >= currDt).filter(Postdata.post_expire == 0).filter(or_(and_(Propertydata.city_id == CityID, Postdata.post_activation_date <= currDt, Postdata.post_expiration_date >= currDt), Postdata.user_id == userid)).order_by(Postdata.id.desc()).limit(limitno).all()

    TenantPostsCnt = db.session.query(Postdata).join(Propertydata, Postdata.post_prop_id == Propertydata.id).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 1).filter(Postdata.post_expiration_date >= currDt).filter(Postdata.post_expire == 0).filter(or_(and_(Propertydata.city_id == CityID, Postdata.post_activation_date <= currDt, Postdata.post_expiration_date >= currDt), Postdata.user_id == userid)).count()
    #TenantPostsCnt = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(User.usertype_id == 1).filter(Postdata.post_activation_date <= currDt).filter(Postdata.post_expiration_date >= currDt).count()
    #.filter(Postdata.user_id == userid)
    for item in PostData:
        if (item.post_date):
            post_date = (datetime.datetime.strptime(str(item.post_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_price==0):
            price = "Free"
        else:
            price = item.post_price

        if (item.post_activation_date):
            post_activation_date = (datetime.datetime.strptime(str(item.post_activation_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_expiration_date):
            post_expiration_date = (datetime.datetime.strptime(str(item.post_expiration_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        FData = []
        FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        for idx, subitem in enumerate(FilesData):
            if (idx==0) :
                new_add = {
                    'file_name': subitem.file_name,
                }
                FData.append(new_add)

        new_link = {
            'id': item.id,
            'user_id': item.user_id,
            'title': item.post_title,
            'price': price,
            'filestoragekey': item.post_storagekey,
            'content': item.post_message,
            'date': post_activation_date,
            'post_expiration_date': post_expiration_date,
            'post_date': post_date,
            'attachFile': FData,
        }

        TenantPostsData.append(new_link)


    ManagementPostsData = []
    if (limitno==0):
        PostData = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(Postdata.user_id == MaID).filter(User.usertype_id == 2).filter(Postdata.post_activation_date <= currDt).filter(Postdata.post_expiration_date >= currDt).order_by(Postdata.id.desc()).all()
    else:
        PostData = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(Postdata.user_id == MaID).filter(User.usertype_id == 2).filter(Postdata.post_activation_date <= currDt).filter(Postdata.post_expiration_date >= currDt).order_by(Postdata.id.desc()).limit(limitno).all()

    ManagementPostsCnt = db.session.query(Postdata).join(User, Postdata.user_id == User.id).filter(Postdata.user_id == MaID).filter(User.usertype_id == 2).filter(Postdata.post_activation_date <= currDt).filter(Postdata.post_expiration_date >= currDt).count()
    #.filter(Postdata.post_pinned_flag == 0)
    for item in PostData:
        if (item.post_date):
            post_date = (datetime.datetime.strptime(str(item.post_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_price==0):
            price = "Free"
        else:
            price = item.post_price

        if (item.post_activation_date):
            post_activation_date = (datetime.datetime.strptime(str(item.post_activation_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        if (item.post_expiration_date):
            post_expiration_date = (datetime.datetime.strptime(str(item.post_expiration_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        FData = []
        FilesData = db.session.query(Postdatafile).filter(Postdatafile.post_id == item.id).all()
        for idx, subitem in enumerate(FilesData):
            if (idx==0) :
                new_add = {
                    'file_name': subitem.file_name,
                }
                FData.append(new_add)

        new_link = {
            'id': item.id,
            'user_id': item.user_id,
            'title': item.post_title,
            'price': price,
            'filestoragekey': item.post_storagekey,
            'content': item.post_message,
            'date': post_activation_date,
            'post_expiration_date': post_expiration_date,
            'post_date': post_date,
            'attachFile': FData,
        }

        ManagementPostsData.append(new_link)

    return jsonify({
        'GeneralPostsData': GeneralPostsData,
        'TenantPostsData': TenantPostsData,
        'ManagementPostsData': ManagementPostsData,
        'GeneralPostsCnt': 0,
        'TenantPostsCnt': TenantPostsCnt,
        'ManagementPostsCnt': ManagementPostsCnt,
    }), HTTP_200_OK

@postdata.delete("/<int:id>/<int:user_id>")
@jwt_required()
def delete_post(id, user_id):

    postinfo = Postdata.query.filter_by(user_id=user_id, id=id).first()

    if not postinfo:
        return jsonify({'message': 'Item not found'}), HTTP_404_NOT_FOUND

    db.session.query(Postdatafile).filter_by(post_id=id).delete(synchronize_session=False)
    db.session.commit()

    db.session.delete(postinfo)
    db.session.commit()

    return jsonify({}), HTTP_204_NO_CONTENT

@postdata.put('/updatepostcron')
@jwt_required()
def updatepostcron():

    TenantsPostExpMonth = float(request.get_json().get('TenantsPostExpMonth', ''))

    PostData = db.session.query(Postdata).join(User, User.id == Postdata.user_id).filter(User.usertype_id == '1').all()
    for PostDataInfo in PostData:
        delta = relativedelta.relativedelta(datetime.datetime.now(), PostDataInfo.created_at)
        diff_months = delta.months + (delta.years * 12)

        if (diff_months>=TenantsPostExpMonth) :
            PostDataInfo.post_expire = '1'
            db.session.commit()

    retData = []
    TenantsConversationDelete = float(request.get_json().get('TenantsConversationDelete', ''))
    ConvData = db.session.query(Conversion).filter(Conversion.post_id != '').filter(Conversion.conv_active == '1').all()
    for ConvDataInfo in ConvData:

        #ConvDataInfo.created_at
        
        postDataInfo = Postdata.query.filter_by(id=ConvDataInfo.post_id).first()
        if not postDataInfo:
            ConvDataInfo.conv_active = '0'
            db.session.commit()
        else:
            postExpirationDate = (datetime.datetime.strptime(str(postDataInfo.post_expiration_date), "%Y-%m-%d"))
            delta = relativedelta.relativedelta(datetime.datetime.now(), postExpirationDate)
            diff_months_post = delta.months + (delta.years * 12)

            #retData.append(postExpirationDate)
            #retData.append(ConvDataInfo.created_at)
            
            if (diff_months_post>=TenantsConversationDelete) :
                ConvDataInfo.conv_active = '0'
                db.session.commit()
            else:
                delta = relativedelta.relativedelta(datetime.datetime.now(), ConvDataInfo.created_at)
                diff_months_post = delta.months + (delta.years * 12)
                if (diff_months_post>=TenantsConversationDelete) :
                    ConvDataInfo.conv_active = '0'
                    db.session.commit()




        #delta = relativedelta.relativedelta(datetime.datetime.now(), ConvDataInfo.created_at)
        #diff_months = delta.months + (delta.years * 12)



    return jsonify({
        'retData': retData,
        'Update': 'Success',
        'message': '',
    }), HTTP_200_OK

@postdata.put('/addbonusposts')
@postdata.patch('/addbonusposts')
@jwt_required()
def addbonusposts():
    prop_id = request.get_json().get('prop_id', '')

    propData = db.session.query(Propertydata).with_entities(Propertydata.id, Propertydata.user_id, User.stripe_api_key, User.comm_percentage).join(User, User.id == Propertydata.user_id).filter(Propertydata.id==prop_id).first()
    stripe_api_key = propData.stripe_api_key

    if (stripe_api_key==''):
        return jsonify({'payment': 'error', 'message': 'Management Stripe Account not created' }), HTTP_200_OK
    
    stripe.api_key = stripe_api_key

    cvcNo = request.get_json().get('cvcNo', '')

    amount = request.get_json().get('BonusPay', '') * 100
    #amount = 50 * 100
    paymentDoneFlag = False

    userPayData = Userpaymethod.query.filter_by(user_id=request.get_json().get('pay_user_id', ''), id=request.get_json().get('selectedCardID', '')).first()

    f = Fernet(userPayData.fernet_key)
    cardDataTok = f.decrypt(userPayData.stripe_token_data).decode()
    cardData = json.loads(cardDataTok)

    ChargeDataID = ''
    TokenInfo = stripe.Token.create(
        card={
            "number": cardData['number'],
            "exp_month": cardData['exp_month'],
            "exp_year": cardData['exp_year'],
            "cvc": cvcNo, #cardData['cvc'],
        },
    )

    try:
        ChargeData = stripe.Charge.create(
            amount=amount,
            currency="usd",
            source=TokenInfo['id'],
            description='',
        )

        ChargeDataID = ChargeData.id

    except stripe.error.InvalidRequestError:
        ChargeDataID = ''

    if (ChargeDataID!=''):
        sYear = request.get_json().get('sYear', '')
        sMonth = request.get_json().get('sMonth', '')
        sDate = request.get_json().get('sDate', '') 

        bonuspost_date = datetime.datetime(int(sYear), int(sMonth), int(sDate))
        bonuspost_date_text = (bonuspost_date).strftime('%B, %Y')

        BonuspostAdd = Bonuspost(
                    bonuspost_user_id=request.get_json().get('pay_user_id', ''), 
                    bonuspost_date=bonuspost_date, 
                    bonuspost_date_text=bonuspost_date_text, 
                    bonuspost_pay=request.get_json().get('BonusPay', ''), 
                    bonuspost_cnt=request.get_json().get('BonusPostNo', ''), 
                    bonuspost_payment_flag='Paid', 
                    bonuspost_payment_date=bonuspost_date, 
                    bonuspost_payment_amount=request.get_json().get('BonusPay', ''),  
                    bonuspost_stripe_charge_id=ChargeDataID
                )
        db.session.add(BonuspostAdd)
        db.session.commit()

        bonuspost_id = BonuspostAdd.id

        paymentDoneFlag = True
    else:
        bonuspost_id = ''
        paymentDoneFlag = False

    if (paymentDoneFlag):
        return jsonify({
            'id': bonuspost_id,
            'ChargeDataID' : ChargeDataID,
            'payment': 'success',
        }), HTTP_200_OK
    else:
        return jsonify({
            'id': bonuspost_id,
            'ChargeDataID' : ChargeDataID,
            'payment': 'error',
            'message': 'Payment Not Done'
        }), HTTP_200_OK

@postdata.get("/bonusposts/<int:userid>")
@jwt_required()
def getbonuspostsdata(userid):

    BonusPostsData = []
    BonusPostInfo = db.session.query(Bonuspost).filter(Bonuspost.bonuspost_user_id == userid).order_by(Bonuspost.id.desc()).all()
    for item in BonusPostInfo:
        if (item.bonuspost_date):
            bonuspost_date = (datetime.datetime.strptime(str(item.bonuspost_date), "%Y-%m-%d")).strftime('%b. %d, %Y')

        new_link = {
            'id': item.id,
            'bonuspost_cnt': item.bonuspost_cnt,
            'bonuspost_pay': item.bonuspost_pay,
            'bonuspost_date_text': item.bonuspost_date_text,
            'bonuspost_date': bonuspost_date,
        }

        BonusPostsData.append(new_link)

    return jsonify({
        'BonusPostsData': BonusPostsData,
    }), HTTP_200_OK


