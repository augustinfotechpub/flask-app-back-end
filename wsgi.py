import os
import sys
from dotenv import load_dotenv
from src import create_app

#sys.path.append("/var/www/apartoappuat.ml/aparto/apartoenv/lib/python3.8/site-packages")
#sys.path.insert(0, "/var/www/apartoappuat.ml/aparto")

load_dotenv()

app = create_app()

if __name__ == "__main__":
    app.run(host='0.0.0.0')
